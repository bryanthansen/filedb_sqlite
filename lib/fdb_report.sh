#!/bin/bash
# Bryant Hansen

# Description: produce a summary report of the files in a storage device
#
# Properties, attributes, and statistics of:
#
#   - Storage Device: id, partition table
#   - Partition: type, flags
#   - Filesystem: bootable OS's
#   - Files and directories: ownership, permissions, inodes, and hashes, ACL's?

# Identify operational stage and base report on the current stage
#
# Stages:
#   1) create database
#   2) scan directories
#   3) scan files
#   4) hash inodes
#   5) produce dedup report
#   6) execute dedup report
#   7) complete
#
# The database will be locked in stages 1 through 3
# Stage 3 is generally the longest stage
# Stage 5 can also be long, depending on the amount of deduplication required

function fdb_report() {
    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  dbdir"
    local DT=$(date "+%Y%m%d_%H%M%S")

    FILEDB_NAME=filedb.sqlite
    MIN_DEDUP_SIZE=8192

    printf "\n${DT}:${ME}:${FUNCTION}: SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR"

    . "$LIB_DIR"/get_dbdir.sh
    . "$LIB_DIR"/flexcat.sh
    . "$LIB_DIR"/get_dbdir.sh
    . "$LIB_DIR"/get_rootdir.sh
    . "$LIB_DIR"/detect_stage.sh
    . "$LIB_DIR"/version.sh
    . "$LIB_DIR"/stats_log.sh
    . "$LIB_DIR"/human_readable.sh

    . "$SQL_DIR"/file_report.sql
    . "$SQL_DIR"/illegal_filenames.sql
    . "$SQL_DIR"/query_total_dup_size.sql
    . "$SQL_DIR"/query_num_inodes_hashed.sql
    . "$SQL_DIR"/query_num_unique_hashes.sql
    . "$SQL_DIR"/query_num_large_inodes_hashed.sql

    unset status_log
    dbdir="$(get_dbdir "$*" || echo "")"
    if [[ ! -d "$dbdir" ]] ; then
        if [[ "$dbdir" ]] ; then
            log "ERROR: $dbdir is not a valid directory"
            exit 80
        else
            log "ERROR: no dbdir found for arguments '$*'"
            exit 81
        fi
    fi
    local filedb="$dbdir"/$FILEDB_NAME

    rootdir="$(get_rootdir "$dbdir")"
    if [[ ! -d "$rootdir" ]] ; then
        log "ERROR: the rootdir does not exist as a directory: '${rootdir}'"
        exit 82
    fi

    log_section_start    "OPERATING PARAMETERS"
    log_label            "args"     "'$*'"
    log_label            "PWD"      "$PWD"
    log_label            "dbdir"    "${dbdir}"
    log_label            "filedb"   "$FILEDB_NAME"
    log_label            "rootdir"  "${rootdir}"
    local stats_file="$dbdir"/stats.log
    touch "$stats_file"
    reldir="${rootdir#${mountpoint}/}"
    log_label            "RELATIVE DIR" "$reldir"
    mountpoint="$(df . --output=target | sed 1d)"
    log_label            "MOUNTPOINT" "${mountpoint}"

    log_label            "DIRECTORY USAGE"
    if [[ "$CHECK_DIR_USAGE" ]] ; then
        # this can require a significant amount of time
        log_append_line "$(du -hs "$rootdir" | cut -f 1)\n"
    else
        log_append_line  "Disabled - too slow/long\n"
    fi
    log_section_end      "OPERATING PARAMETERS"
    write_stats "$stats_file" enter $FUNCTION

    log_section_start    "VERSION"
    log_label            "Git Branch"
    log_append_line "${GIT_BRANCH}"
    if [[ "$GIT_IS_CLEAN" == true ]] ; then
        log_append_line  " (clean)\n"
    else
        log_append_line  " (dirty)\n"
    fi
    log_label            "Git Date"          "$GIT_COMMIT_DATE"
    log_label            "Git Hash"          "${GIT_HASH:0:8}"
    log_label            "Revision Num"      "$GIT_REVISION"
    log_label            "Uncommitted Lines" "$GIT_NUM_UNSTAGED_LINES"
    log_section_end      "VERSION"

    log_section_start    "STORAGE DEVICE"
    local device="$(df "$rootdir" --output=source | sed 1d)"
    log_label            "DEVICE" "${device}"
    log                  "TODO: get more device information"
    log_section_end      "STORAGE DEVICE"

    log_section_start    "PARTITION"
    # Notes about requesting specific parameters from df:
    #      FELDLISTE ist eine Komma-getrennte Liste von Spalten, die
    #      eingeschlossen werden  sollen.
    #      Gültige  Feldnamen sind:
    #        source,  fstype,  itotal, iused, iavail, ipcent, size, used,
    #        avail, pcent, file und target
    params="target fstype size used pcent avail"
    for param in $params ; do
        df_res=$(df -h $device --output=$param | sed 1d)
        df_res="${df_res//%/%%}"
        df_res="${df_res#"${df_res%%[![:space:]]*}"}"
        log_label "$param" "$df_res"
    done
    ipcent=$(df -h $device --output=ipcent | sed 1d)
    if [[ "${ipcent%\%}" -gt 85 ]] ; then
        log "WARNING: ${ipcent}%% of inodes are used\n"
        params="iused iavail itotal"
        for param in $params ; do
            df_res=$(df -h $device --output=$param | sed 1d)
            df_res="${df_res//%/%%}"
            log_label "$param" "$df_res "
        done
    fi
    log_section_end      "PARTITION"

    log_section_start    "FILEDB EXECUTION STAGE"
    stage="$(detect_stage "$dbdir")"
    log_label            "detect_stage" "$stage"
    log_section_end      "FILEDB EXECUTION STAGE"
    write_stats "$stats_file" stage "$stage"

    if [[ "$stage" -ge 1 ]] ; then
        log_section_start "DATABASE"
        if [[ ! "$dbdir" ]] ; then
            log "ERROR: dbdir not specified"
            return 4
        fi

        db_is_locked=1
        if [[ -f "$filedb" ]] ; then
            log_label "dbdir"   "$(dirname  "$filedb")"
            log_label "DB File" "$(basename "$filedb") (exists)"
            db_size="$(stat --format=%s "$filedb")"
            db_size_hr="$(human_readable "$db_size")"
            log_label "DB Size" "$db_size bytes (${db_size_hr}B)"
            log_label "filedb start time" "TODO"
            db_status="$(sqlite3 "$filedb" .schema 2>&1 \
                      || echo "error_code=$?")"
            if [[ "$db_status" == "Error: database is locked" ]] ; then
                log_label "lock_status" "locked"
            else
                schema_lines="$(echo -e "$db_status" | wc -l)"
                if [[ "$schema_lines" -lt 3 ]] ; then
                    log "ERROR: failed to fetch filedb schema. " \
                        "Message: $db_status"
                else
                    log_label "schema_lines" "$schema_lines"
                    log_label "DB lock status" "unlocked"
                    unset db_is_locked
                fi
            fi
            log_label "update-in-progress" "TODO"
        elif [[ -f "${filedb}.xz" ]] ; then
            log "database is compressed;" \
                "unxz ${filedb}.xz if you want to use it"
        elif [[ -f "${filedb}.gz" ]] ; then
            log "database is compressed;" \
                   "gunzip ${filedb}.gz if you want to use it"
        else
            log_label "filedb" "$(readlink -f "$filedb") (missing)"
            return 5
        fi
        log_section_end  "DATABASE"
    fi

    if [[ "$stage" -ge 2 ]] ; then
        log_section_start "DATABASE - DIRECTORIES"
        unset MEASURE_SCAN_RATE
        if [[ -f "$filedb" && ! "$db_is_locked" ]] ; then
            log_label    "Directory Records"
            max_dir_records="$(
                sqlite3 "$filedb" "SELECT MAX(id) FROM directories;"
            )"
            log_append_line "${max_dir_records}\n"
        fi
        [[ "$max_dir_records" ]] || max_dir_records=0
        if [[ "$max_dir_records" == 0 && "$stage" -gt 2 ]] ; then
            log "ERROR: detect_stage indicates stage ${stage}, but no" \
                "directory records were found in the DB"
            return 6
        fi

        dir_scan_log="$dbdir"/filedb.sqlite.dirs.sql
        if [[ ! -f "$dir_scan_log" ]] ; then
            unset MEASURE_SCAN_RATE
            if [[ ! -f "$dir_scan_log".gz \
              && ! -f "$dir_scan_log".xz ]] ; then
                log "INFO: dir_scan_log $dir_scan_log does not exist"
                unset dir_scan_log
            fi
        fi
        if [[ "$dir_scan_log" ]] ; then
            # if we know that we're running, then turn on MEASURE_SCAN_RATE

            # Read the total number of dirs from the logfile, if it exists and
            # the value appears valid
            log_dirs=$(read_stats "$stats_file" first value dirs)

            if [[ "$log_dirs" ]] ; then
                dirs="$log_dirs"
                t1="$(read_stats "$stats_file" first timestamp dirs)"
                now=$(date +%s)
                let ago=now-t1
                #log "directories" "$dirs -- ${stats_file} lookup" \
                #    "timestamp: $t1 ($ago seconds ago)"
                log_label "Directories Logged"
                log_append_line "$dirs $(human_readable "$log_dirs") -- ${stats_file} lookup\n"
                log_label "Dir Scan Timestamp"
                log_append_line "$t1 ($ago seconds ago)\n"
            else
                log_label "Num Dirs"
                dirs="$(
                    flexcat "$dir_scan_log" 2>/dev/null \
                    | grep "^'" \
                    | wc -l \
                    | cut -f 1
                )"
                log_append_line "${dirs} -- ${dir_scan_log} count\n"
                write_stats "$stats_file" dirs "$dirs"
                t1="$(date +%s)"
                if [[ "$MEASURE_SCAN_RATE" ]] ; then
                    # sleep for a little bit so we can gauge approximately how
                    # fast we're scanning
                    sleep 10
                fi
            fi
            if [[ "$MEASURE_SCAN_RATE" ]] ; then
                dirs2="$(
                    flexcat "$dir_scan_log" 2>/dev/null \
                    | grep "^'" \
                    | wc -l \
                    | cut -f 1
                )"
                t2="$(date +%s)"
                let num_dirs=dirs2-dirs
                let elapsed=t2-t1
                if [[ "$num_dirs" -gt 0 ]] ; then
                    let rate=num_dirs/elapsed
                    log_label "dirscan rate" "$rate directories per second"
                else
                    log "directory scan does not appear to be running;" \
                        "no new dirs in $elapsed seconds"
                fi
            fi
        fi
        log_section_end  "DATABASE - DIRECTORIES"
    fi

    if [[ "$stage" -ge 3 ]] ; then
        log_section_start "DATABASE - FILES"
        unset MEASURE_SCAN_RATE
        unset is_sharded

        if [[ -f "$filedb" && ! "$db_is_locked" ]] ; then
            # check if we've completed the file scanning phase
            log_label    "File Records"
            db_num_files="$(sqlite3 "$filedb" "SELECT COUNT(id) FROM files;")"
            if [[ "$db_num_files" -gt 0 ]] ; then
                log_append_line "${db_num_files}" \
                                "($(human_readable "$db_num_files"))\n"
            else
                log_append_line "INFO: The database contains no file records\n"
            fi

            log_label    "Last dir scan record"
            db_max_file_dirid="$(
                sqlite3 "$filedb" "SELECT MAX(dirid) FROM files"
            )"
            [[ "${db_max_file_dirid}" ]] || db_max_file_dirid=0
            log_append_line "${db_max_file_dirid}\n"

            pcnt_x_10="$(expr $db_max_file_dirid \* 1000 / $max_dir_records)"
            log_label "Percent Complete" "${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}"

            log_label    "Num Inodes"
            num_inodes="$(
                sqlite3 "$filedb" "SELECT COUNT(DISTINCT inode) FROM files"
            )"
            log_append_line "${num_inodes}\n"

            log_label    "Num Large Inodes"
            num_large_inodes="$(
                sqlite3 \
                    "$filedb" \
                    "SELECT COUNT(DISTINCT inode)
                     FROM files
                     WHERE size >= ${MIN_DEDUP_SIZE}"
            )"
            log_append_line "$num_large_inodes" \
                "(>= ${MIN_DEDUP_SIZE} bytes)\n"

            log_label    "Illegal filenames"
            num_illegal_files="$(
                                    sqlite3 \
                                        "$filedb" \
                                        "$sql_num_illegal_filenames" \
                                    || echo -n "(sqlite3 failure) "
                                )"
            [[ "$num_illegal_files" ]] || num_illegal_files=0
            log_append_line "${num_illegal_files}\n"
            if [[ "$num_illegal_files" -gt 0 ]] ; then
                illegal_filename_report="${dbdir}"/illegal_filenames_${DT}.err
                echo " # $num_illegal_files files with illegal characters" \
                     "in the name.  (currently only filtering for LF (0xA))" \
                | tee "$illegal_filename_report" \
                >&2
                echo " # Producing report at $illegal_filename_report" >&2
                sqlite3 "$filedb" "$sql_illegal_filenames" \
                >> "$illegal_filename_report"
            fi

        fi
        [[ "$db_num_files" ]] || db_num_files=0
        [[ "$db_max_file_dirid" ]] || db_max_file_dirid=0

        if [[ "$db_max_file_dirid" -lt "$max_dir_records" ]] ; then
            # we are not finished scanning files into the DB
            # determine how many files have been scanned into our logs

            if ls "$dbdir"/filedb.sqlite.files.sql > /dev/null 2>&1 \
            || ls "$dbdir"/filedb.sqlite.files.sql.[gx]z > /dev/null 2>&1
            then
                log_label "file records in logs"
                num_files_in_log="$(
                    flexcat "$dbdir"/filedb.sqlite.files.sql 2>/dev/null \
                    | grep -c "^INSERT"
                )"
                log_append_line "${num_files_in_log}\n"

                log_label "max dirid in logfiles"
                max_dirid_in_log="$(
                    flexcat "$dbdir"/filedb.sqlite.files.sql 2>/dev/null \
                    | grep -A 1 "^[ \t]*VALUES" \
                    | grep -v "^[ \t]*VALUES\|^--" \
                    | sed "s/^[ \t]*//;s/\,.*//;/^[ \t]*$/d" \
                    | tail -n 1
                )"
                log_append_line "${max_dirid_in_log}\n"
            fi
            [[ "$num_files_in_log" ]] || num_files_in_log=0
            [[ "$max_dirid_in_log" ]] || max_dirid_in_log=0

            if ls "$dbdir"/filedb.sqlite.files.[0-9]*.sql  > /dev/null 2>&1 \
            || ls "$dbdir"/filedb.sqlite.files.[0-9]*.sql.[gx]z > /dev/null 2>&1
            then
                log_label "file records in sharded logfiles"
                num_files_in_log_shards="$(
                    ls -1 \
                        "$dbdir"/filedb.sqlite.files.[0-9]*.sql \
                        "$dbdir"/filedb.sqlite.files.[0-9]*.sql.[gx]z \
                        2>/dev/null \
                    | while read logf ; do
                        # TODO: if both the compressed and uncompressed
                        #       versions exist, use only the uncompressed
                        flexcat "$logf" 2>/dev/null
                    done \
                    | grep -c "^INSERT"
                )"
                log_append_line "${num_files_in_log_shards}\n"

                log_label "max dirid in sharded logfiles"
                max_dirid_in_log_shards="$(
                    ls -1tr \
                        "$dbdir"/filedb.sqlite.files.[0-9]*.sql \
                        "$dbdir"/filedb.sqlite.files.[0-9]*.sql.[gx]z \
                        2>/dev/null \
                    | tail -n 1 \
                    | while read logf ; do
                        # TODO: if both the compressed and uncompressed
                        #       versions exist, use only the uncompressed
                        flexcat "$logf"
                    done \
                    | grep -A 1 "^[ \t]*VALUES" \
                    | grep -v "^[ \t]*VALUES\|^--" \
                    | sed "s/^[ \t]*//;s/\,.*//;/^[ \t]*$/d" \
                    | tail -n 1
                )"
                log_append_line "${max_dirid_in_log_shards}\n"
                if [[ "$max_dirid_in_log_shards" -gt "$max_dirid_in_log" ]]
                then
                    max_dirid_in_log="$max_dirid_in_log_shards"
                    is_sharded=1
                fi
            fi
            log_label "max dirid in logs" "$max_dirid_in_log"
        fi

        log_section_end  "DATABASE - FILES"
    fi

    if [[ "$stage" -ge 4 ]] ; then
        log_section_start "DATABASE - HASHES"
        MEASURE_HASH_RATE=1

        if [[ -f "$filedb" && ! "$db_is_locked" ]] ; then
            # Check if we've completed the hashing phase before we start
            # calculating duplicates

            log_label    "File Hash Records"
            num_hashes="$(
                sqlite3 "$filedb" "SELECT COUNT(id) FROM hashes;"
            )"
            [[ "$num_hashes" ]] || num_hashes=0
            log_append_line "${num_hashes} $(human_readable "$num_hashes")\n"

            if [[ "$num_hashes" -gt 1 ]] ; then
                # we've at least started

                printf "\n" >&2
                log      "Top 10 duplicate entries by duplicate size:"
                sqlite3 -header -separator " "  "$filedb" "$sql_file_report" \
                | while read md5 inode size num_dups dup_size name ; do
                    printf "# %32s %12s %10s %4s %10s %s\n" \
                           "$md5" \
                           "$inode" \
                           "$(human_readable "$size" 2>/dev/null)B" \
                           "$num_dups" \
                           "$(human_readable "$dup_size" 2>/dev/null)B" \
                           "$name"
                done >&2
                printf "\n" >&2

                # If we assume there is only 1 inode per hash, and not multiple
                # identical inodes with different timestamps, then we can
                # count the number of duplicate inode content-hashes by
                # subtracting the number of inodes from the number of unique
                # hashes.

                # TODO: Decided if multiple inodes with different timestamps
                #       should be supported.  Will the scan be a 1-time
                #       operation, or will it be a living dataset

                log_label "Num Inodes Hashed (>=8192)"
                num_inodes_hashed="$(
                    sqlite3 "$filedb" "$sql_num_large_inodes_hashed"
                )"
                log_append_line "${num_inodes_hashed}\n"

                log_label "Num Unique Hashes (>=8192)"
                num_hashes="$(
                    sqlite3 "$filedb" "$sql_num_unique_hashes"
                )"
                log_append_line "${num_hashes}\n"

                let num_duplicates=num_inodes_hashed-num_hashes
                log_label "Num Duplicate Inodes (>=8192)" "$num_duplicates"

                log "$num_duplicates inodes have redundant hashes" \
                    "(ie. are duplicates)"

                log_label "Total Dup Size"
                total_dup_size="$(sqlite3 "$filedb" "$sql_total_dup_size")"
                log_append_line "$total_dup_size "
                log_append_line "($(human_readable "$total_dup_size")B)\n"
            fi
        fi

        hashes_list="$dbdir"/filedb.sqlite.hashes.sql
        if [[ ! -f "$hashes_list" ]] ; then
            unset MEASURE_HASH_RATE
            if [[ ! -f "$hashes_list".gz \
               && ! -f "$hashes_list".xz ]] ; then
                log "INFO: hash list $hashes_list (and compressed forms)" \
                    "not found (stage=${stage})"
                unset hashes_list
            fi
        fi

        if [[ "$hashes_list" ]] ; then
            log_label "File Hashes Logged"
            num_hashed="$(flexcat "$hashes_list" 2>/dev/null \
                          | sed "/^COMMIT.*/d;/^BEGIN\ TRANSACTION.*/d" \
                          | wc -l \
                          | cut -f 1
                        )"
            t1="$(date +%s)"
            log_append_line "${num_hashed} of ${num_inodes}\n"

            pcnt_x_10="$(expr $num_hashed \* 1000 / $num_inodes)"
            log_label "Percent Complete" "${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}"

            log_label "last line"
            last_line="$(flexcat "$hashes_list" 2>/dev/null \
                         | tail \
                         | tr -d '\0' \
                         | sed "/^[ \t]*$/d;s/\ \+/\ /g" \
                         | tail -n 1
                        )"
            match_insert="INSERT INTO hashes ( inode, md5, timestamp ) VALUES ( * );"
            log_append_line "${last_line}\n"
            if [[ ! "$last_line" ]] ; then
                log "The last line of the hash log is NULL - script error"
            elif [[ ! "${last_line//COMMIT;/}" ]] ; then
                log "The last line is a complete commit message"
            elif [[ ! "${last_line//${match_insert}/}" ]] ; then
                log "The last line is a complete SQL file hash record"
            else
                log "ERROR: the last line of the hash log does not appear" \
                       "to be valid, complete, or expected SQL statement:" \
                       "'${last_line}'"
            fi

            if [[ "$MEASURE_HASH_RATE" ]] ; then
                log_label "file hash rate"
                sleep 10
                num_hashed2="$(flexcat "$hashes_list" 2>/dev/null \
                              | sed "/^COMMIT.*/d;/^BEGIN\ TRANSACTION.*/d" \
                              | wc -l \
                              | cut -f 1
                            )"
                t2="$(date +%s)"
                let elapsed=t2-t1
                if [[ "$num_hashed2" -gt "$num_hashed" ]] ; then
                    # we're running
                    let num_hashed=num_hashed2-num_hashed
                    let rate=num_hashed/elapsed
                    log_append_line "$rate hashes-per-second\n"
                elif [[ "$num_hashed2" == "$num_hashed" ]] ; then
                    log_append_line "no new hashes in $elapsed seconds\n"
                else
                    log_append_line "\n"
                    log "ERROR: the number of hashes has apparently" \
                        "decreased from $num_hashed to ${num_hashed2}"
                fi
            fi

        fi

        log_section_end  "DATABASE - HASHES"
    fi

    if [[ "$stage" -ge 5 ]] ; then
        log_section_start "DUPLICATE REPORT"
        dup_report="$dbdir"/duplicate_report.sh
        if [[ \
              ! -f "$dup_report"    \
          && ! -f "$dup_report".gz \
          && ! -f "$dup_report".xz ]]
        then
            log          "INFO: dup_report $dup_report does not exist"
            unset dup_report
        fi
        if [[ "$dup_report" ]] ; then
            dup_report_lines="$(
                                  flexcat "$dup_report" \
                                  | grep -c "^ln\ " "$dup_report"
                                )"
            log_label    "Dup Report Lines" "$dup_report_lines"
            dup_report_size="$(stat --format=%s "$dup_report")"
            log_label    "Dup Report Size" "$dup_report_size bytes"
        fi
        log_section_end  "DUPLICATE REPORT"
    fi

    if [[ "$stage" -ge 6 ]] ; then
        log_section_start "DE-DUPLICATION EXECUTION"
        if ls -1 "$dbdir"/execute*.log 2>/dev/null ; then
            printf "$(
                          ls -1 "$dbdir"/execute*.log | wc -l | cut -f 1
                      ) execute logs found\n" >&2
        else
            if ls -1 "$dbdir"/execute*.log.xz 2>/dev/null ; then
                printf "$(
                              ls -1 "$dbdir"/execute*.log.xz | wc -l | cut -f 1
                        ) execute logs found\n" >&2
            else
                log "No execute logs found\n"
            fi
        fi
        log_section_end  "DE-DUPLICATION EXECUTION"
    fi

    write_stats "$stats_file" exit $FUNCTION

    log "Complete."

    return 0
}

# way too huge; should split this up
