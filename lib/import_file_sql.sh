#!/bin/sh
# Bryant Hansen

# Description: Import the already-produced SQL statements into the file table
#              of the DB

function import_file_sql() {

    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  filedb"
    local filedb="$1"

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh
    . "$LIB_DIR"/log_last_dir_scan.sh

    local db_max_dirid="$(sqlite3 "$filedb" "SELECT MAX(id) FROM directories")"
    [[ "$db_max_dirid" ]] || db_max_dirid=0

    local db_max_file_dirid="$(sqlite3 "$filedb" "SELECT MAX(dirid) FROM files")"
    [[ "$db_max_file_dirid" ]] || db_max_file_dirid=0

    if [[ "$db_max_file_dirid" -gt "$db_max_dirid" ]] ; then
        log "ERROR: The max directory id $db_max_file_dirid in the file" \
            "table should not be greater than the max directory id in the" \
            "directories table $db_max_dirid"
        exit 40
    fi

    if [[ "$db_max_dirid" == "$db_max_file_dirid" ]] ; then
        log "We have completed directory scanning in the DB.  This stage is" \
        "complete"
        return 0
    fi

    if [[ "$db_max_file_dirid" -gt 0 ]] ; then
        # resume operation
        unset import_from_beginning
    else
        import_from_beginning=1
    fi

    # Update the database
    if ls "$filedb".files.*.sql* > /dev/null 2>&1 ; then
        # Requirement:
        # If both the compressed and uncompressed file exist, only the
        # uncompressed will be used for all files
        #
        # Requirement:
        # If we are resuming a DB import, check what files we have already
        # imported
        #
        # Requirement:
        # If no shards have already been partially-imported, resume with the
        # next shard
        #
        # Requirement:
        # If a shard has been partially imported, then only delete the DB
        # records matching that shard and re-import

        # loop through all shards, compressed or uncompressed
        for f in "$filedb".files.*.sql "$filedb".files.*.sql.[gx]z ; do
            if [[ ! -f "$f" ]] ; then
                continue
            fi
            # If both compressed and uncompressed shards exist, only use the
            # uncompressed
            basename="${f%.[gx]z}"
            if [[ "$f" != "$basename" && -f "$basename" ]] ; then
                log "Skip $f since $basename is scanned"
                continue
            fi
            last_dirid_in_shard=$(log_last_dir_scan "$f")
            if [[ "$last_dirid_in_shard" -gt "$db_max_dirid" ]] ; then
                log "ERROR: the last directory in the shard is ${last_dirid_in_shard}," \
                    "but the max directory id in the DB is ${db_max_dirid}. " \
                    "Invalid directory id"
                exit 14
            elif [[ "$last_dirid_in_shard" == "$db_max_file_dirid" ]] ; then
                # We're in luck
                # We have terminated after a complete import of a shard
                # We can simply continue at the next shard
                log "Start Importing at Next Shard"
                import_from_beginning=1
            elif [[ "$last_dirid_in_shard" -gt "$db_max_file_dirid" ]] ; then
                if [[ "$import_from_beginning" ]] ; then
                    log "Import shard $(basename "${f}")"
                    printf "    " >&2
                    unset fdout
                    [[ "$VERBOSE" ]] && fderr=/dev/stderr || fderr=/dev/null
                    flexcat "$f" 2> $fderr | sqlite3 "$filedb" 2>> "$f".err
                    echo >&2
                    if [[ -s "$f".err ]] ; then
                        log "Shard written to DB; some errors occurred. " \
                            "see ${f}.err"
                    else
                        log "Shard written to DB"
                        rm "$f".err
                    fi
                    log_header
                    printf "%20s: " "DB Num File Records" >&2
                    num_files="$(
                            sqlite3 "$filedb" "SELECT COUNT(id) FROM files"
                    )"
                    log_append_line "${num_files}\n"
                    log_header
                    printf "%20s: " "DB Last Dir Scanned" >&2
                    num_dirs_scanned="$(
                            sqlite3 "$filedb" "SELECT MAX(dirid) FROM files"
                    )"
                    log_append_line "${num_dirs_scanned}\n"

                    # Validation Checks
                    #log "Verifing that the max dir id in the file table is" \
                    #    "not greater than the max dir ID of the shard imported"
                    if [[ "$db_max_file_dirid" -gt "$num_dirs_scanned" ]] ; then
                        log "ERROR: we have a directory ID in the file table" \
                            "that is much-too-large.  Abort"
                        exit 15
                    fi
                    log_header
                    printf "%20s: " "DB Max File ID" >&2
                    max_file_id="$(
                            sqlite3 "$filedb" "SELECT COUNT(id) FROM files"
                    )"
                    #log "Verify that the number of files is the same as" \
                    #    "the max file id - no gaps and no duplicates"
                    log_append_line "${max_file_id}\n"
                    if [[ "$max_file_id" -ne "$num_files" ]] ; then
                        log "WARNING: max_file_id=${max_file_id}," \
                            "num_files=${num_files}.  This should be 1-to-1. " \
                            "We have duplicates and/or holes"
                    fi
                    log_header
                    local db_size_bytes="$(stat --format=%s "$filedb")"
                    printf "%20s: %s\n" "DB Size" "$db_size_bytes" >&2
                    if [[ "$last_dirid_in_shard" == "$db_max_dirid" ]] ; then
                        log "It appears that the import process is complete."
                        log "All $db_max_dirid directories have been scanned" \
                            "and the file data logged"
                    fi
                else
                    # We've passed the DB and we did not find our ID at the
                    # tail end of a shard
                    # We appear to have a partially-imported shard
                    # Should we:
                    #   - flush the file table and re-import everything?
                    #   - flush the records contained in the current shard and
                    #     continue here
                    # 
                    # If we want to continue our oWe should probably:
                    # 1) get the start ID of the shard
                    # 2) remove all file records in the DB with a dirid greater
                    #    than the start ID of the shard
                    # 3) re-import the shard
                    log "WARNING: Partial shard import detected. " \
                        "last_dirid of shard $f is ${last_dirid_in_shard};" \
                        "the last dirid scanned is $db_max_file_dirid"
                    log "Temporary exit" \
                        "- missing Requirement to re-import shard"
                    log "TODO: implement me!" \
                        "  For now, your best bet would be to manually " \
                        "delete records from the db back to the boundary of" \
                        "the last shard"
                    exit 16
                fi
            else
                # continue advancing
                log_header
                printf "%20s: %s\n" "db_max_file_dirid" "${db_max_file_dirid}" >&2
                log_header
                printf "%20s: %s\n" "last_dirid_in_shard" "${last_dirid_in_shard}" >&2
                log "Shard already imported"
            fi
        done
    elif ls "$filedb".files.sql > /dev/null 2>&1 \
      || ls "$filedb".files.sql.[gx]z > /dev/null 2>&1
    then
        # Import un-sharded (monolithic) files
        if [[ "$import_from_beginning" ]] ; then
            f="$filedb".files.sql
            log "Incorporating $f into the DB"
            flexcat "$f" | sqlite3 "$filedb" 2>> "$filedb".err
            [[ -s "$f".err ]] || rm "$f".err
        else
            # resume operation
            log "ERROR: import_from_beginning not set with monolithic,"
                "unsharded file.  TODO: delete all file records and re-import"
            exit 17
        fi
    else
        log "ERROR: no '$filedb'.files.sql* files found; cannot resume file " \
            "table import"
        exit 18
    fi
    log "File Scan Complete"
    log_label "file records in db" "$(
            sqlite3 "$filedb" "SELECT COUNT(id) FROM files"
    )"
    log_label "Last directory id" "$(
        sqlite3 "$filedb" "SELECT max(id) FROM directories"
    )"
    log_label "Last dir scanned" "$(
        sqlite3 "$filedb" "SELECT max(dirid) FROM files"
    )"
    log_label "FileDB Size" "$(stat --format="%s" "$filedb")"
}
