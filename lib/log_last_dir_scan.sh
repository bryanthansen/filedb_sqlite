#!/bin/sh
# Bryant Hansen

# Description:
# Dump the last valid directory ID from an fdb SQL logfile containing file
# records
# If we are passed a directory, we will look for the last shard found, sorted
# by filename.
# If we are passed a file, we will just return the last dir ID for that file

function log_last_dir_scan() {
    set -e
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh
    . "$LIB_DIR"/last_file_shard_id.sh
    . "$LIB_DIR"/backup_file_and_truncate_original.sh
    . "$LIB_DIR"/last_valid_line.sh

    log "Enter.  PWD=$PWD  ARGS: '$*'"

    if [[ -f "$1" ]] ; then
        local dbdir="$(dirname "$1")"
        local shard_file="$1"
        local filedb="$dbdir"/files.sqlite
        if [[ ! -f "$1" ]] ; then
            log "ERROR: $1 does not exist as a file"
            return 30
        fi
        log "Check shard $(basename "$shard_file")"
    elif [[ -d "$1" ]] ; then
        local dbdir="$1"
        local filedb="$dbdir"/filedb.sqlite
        log_header
        printf "%20s: " "Last Shard ID" >&2
        local last_shard_id="$(last_file_shard_id "$filedb" 2>/dev/null)"
        log_append_line "${last_shard_id}\n"
        [[ "$last_shard_id" ]] || last_shard_id=0
        if [[ "$last_shard_id" -le 0 ]] ; then
            log "No file scan shards found; check for unsharded files"
            if ls "$filedb".files.sql > /dev/null 2>&1 \
            || ls "$filedb".files.sql.[gx]z > /dev/null 2>&1
            then
                log "Unsharded file found"
                local shard_file="$last_shard_file"
            else
                log "No unsharded SQL files found: ${filedb}.files.sql*"
                echo 0
                return 0
            fi
        else
            last_shard_file="$(
                ls -1 "$filedb".files.[0-9]*.sql.[gx]z \
                | tail -n 1
            )"
            log "Last Shard: $(basename "$last_shard_file")"
        fi
        # Prefer an uncompressed file, if it exists
        if [[ -f "${last_shard_file%%.[gx]z}" ]] ; then
            last_shard_file="${last_shard_file%%.[gx]z}"
        fi
        if [[ ! -f "$last_shard_file" ]] ; then
            log "ERROR: last_shard_file $last_shard_file does not exist"
            exit 31
        fi
        local shard_file="$last_shard_file"
    else
        printf "ERROR: $1 does not exist as a file or directory\n" >&2
        exit 32
    fi
    local log_last_dir_scan=0

    # First count the number of lines
    local num_lines=0
    log_header "Number of lines in ${shard_file}: "
    num_lines="$(flexcat "$shard_file" 2>/dev/null | wc -l)"
    log_append_line "${num_lines}\n"

    # Next get the last valid line
    last_valid_line_num="$(get_last_valid_line_num "$shard_file" "$num_lines")"
    if [[ ! "$last_valid_line_num" ]] ; then
        log "ERROR: no valid lines found in shard_file $shard_file"
        exit 33
    fi

    # Then truncate the file if the last line is not valid
    if [[ "$last_valid_line_num" != "$num_lines" ]] ; then
        last_valid_line_num="$(
            backup_file_and_truncate_original \
                "$shard_file" \
                "$last_valid_line_num" \
                "$num_lines"
        )"
        # set the new number of lines to be the same as the last valid line
        num_lines=$last_valid_line_num
    fi
    if [[ ! "$last_valid_line_num" || "$last_valid_line_num" -le 0 ]] ; then
        log "ERROR: failed truncating shard_file $shard_file"
        exit 35
    fi

    log_label "File Recs in Shard"
    local num_records="$(flexcat "$shard_file" 2>/dev/null \
                         | head -n $last_valid_line_num \
                         | grep -c "^INSERT INTO files "
                        )"
    log_append_line "${num_records}\n"

    log_label "Hash Recs in Shard"
    local num_records="$(flexcat "$shard_file" 2>/dev/null \
                         | head -n $last_valid_line_num \
                         | grep -c "^INSERT INTO hashes "
                        )"
    log_append_line "${num_records}\n"

    log_label "First Dir in Shard"
    logfile_first_dir_in_shard="$(
        flexcat "$shard_file" 2>/dev/null \
        | head -n 20 \
        | tr "(," "\n" \
        | sed 's/^[ \t]*//;/^[ \t]*$/d;1,/^VALUES/d' \
        | sed -n 1p
    )"
    log_append_line "${logfile_first_dir_in_shard}\n"

    # now calculate the last valid dirid in the log file
    log_label "Last Dir in Shard" >&2

    #TRUNCATE=1
    unset TRUNCATE
    if [[ "$TRUNCATE" ]] ; then
        log_last_dir_scan="$(
            flexcat "$shard_file" 2>/dev/null \
            | head -n "$last_valid_line_num" \
            | tail \
            | grep -A 1 "^[ \t]*VALUES" \
            | grep -v "^[ \t]*VALUES" \
            | sed "s/^[ \t]*//;s/\,.*//"
        )"
        log_append_line "${log_last_dir_scan}\n"
    else
        log_last_dir_scan="$(
            flexcat "$shard_file" 2>/dev/null \
             | tail -n 50 \
             | tr "(," "\n" \
             | sed "/^[ \t]*$/d;s/^[ \t]*//" \
             | grep -A 1 "INSERT INTO files\|^VALUES" \
             | grep -A 4 "^INSERT INTO files " \
             | tail -n 1
        )"
        log_append_line "${log_last_dir_scan}\n"
    fi
    if [[ ! "$log_last_dir_scan" ]] ; then
        log "ERROR: failed to read a valid line from ${shard_file}"
        exit 37
    fi
    if [[ "$log_last_dir_scan" -le 0 ]] ; then
        log "ERROR: failed to get the last valid directory ID from " \
            "the last shard in ${shard_file}"
        exit 38
    fi
    echo $log_last_dir_scan
}
