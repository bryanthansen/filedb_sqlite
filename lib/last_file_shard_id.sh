#!/bin/sh
# Bryant Hansen

# Description:
# Get the number of the last shard file

function last_file_shard_id() {
    local filedb="$1"
    local last_shard_id="$(
        ls -1 "$filedb".files.[0-9]*.sql.gz 2>/dev/null \
        | sed "s/\.sql\.gz$//;s/.*filedb\.sqlite\.files\.0*//" \
        | sort -n \
        | tail -n 1
    )" || last_shard_id=0
    echo "# last_file_shard_id(${filedb}.files.[0-9]*.sql.gz): last_shard_id='${last_shard_id}'" >&2
    echo "$last_shard_id"
}
