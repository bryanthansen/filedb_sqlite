#!/bin/bash

# Description

# Determine the line number containing the last valid line in an SQL file
# log

# This will be used by the calling program to truncate that that file and
# use the valid portion of the data

# This function will return a '0' on stdout on failure
# On success, it will return the line number, followed by a space, followed by
# the text of the last line (minus leading and trailing whitespace)

function get_last_valid_line_num() {

    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  file"

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh

    local file="$1"
    bn="$(basename "$file")"
    if [[ "$bn" && ! ${bn##filedb.sqlite.files.[0-9]*} ]] ; then
        # We have a valid basename and that matches the pattern

        local last_line="$(flexcat "$file" 2>/dev/null \
              | tail \
              | sed "/^[ \t]*$/d" \
              | tail -n 1 \
              | sed "s/^[ \t]*//"
              )"
        log "last_line: $last_line"
        if [[ "$last_line" == ");" ]] || [[ "$last_line" == "COMMIT;" ]] ; then
            log "The last line is a valid line.  TODO: get the last " \
                "dirid and continue at that point"
            echo $num_lines
            return 0
        else
            log_header "The last line is invalid; last valid line: "
            last_valid_line="$(flexcat "$file" 2>/dev/null \
                                | grep -n "^[ \t]*)\;$" \
                                | tail -n 1
                              )"
            log_append_line "${last_valid_line}\n"
            last_valid_line_num="${last_valid_line%%:*}"
            if [[ "$last_valid_line_num" -gt 0 ]] ; then
                echo "$last_valid_line_num"
            else
                log "ERROR: cannot find the last valid line number: " \
                    "last_valid_line=${last_valid_line}"
                exit 5
            fi
        fi
    fi
}
