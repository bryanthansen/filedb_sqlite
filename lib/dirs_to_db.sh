#!/bin/sh
# Bryant Hansen

# Description: Scan all directories in a directory tree and record metadata
#              to the filedb

# Do not do any scanning if records already exist.  This process is not
# easily-recoverable and it is one of the shorter stages in the process

# This part of the operation is pipelined; data is read and written to the
# filedb simultaneously.  The read operation is read-only and the write
# occurs with transactions

# TODO: have hardcoded excludes of /sys and /proc if we're in a root filesystem

function dirs_to_db() {
    set -e
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh

    local filedb="$1"
    log "filedb: $filedb"

    # TODO: do not scan if we already have any entries in the DB
    num_directories="$(sqlite3 "$filedb" "SELECT COUNT(id) FROM directories")"
    [[ "$num_directories" ]] || num_directories=0
    if [[ "$num_directories" -gt 0 ]]  ; then
        log "ERROR: $num_directories directories already exist in the DB.  A" \
            "new scan makes no sense"
        exit 4
    fi
    num_files="$(sqlite3 "$filedb" "SELECT COUNT(id) FROM files")"
    [[ "$num_files" ]] || num_files=0
    if [[ "$num_files" -gt 0 ]]  ; then
        log "ERROR: $num_files directories already exist in the DB.  A new" \
            "scan makes no sense"
        exit 4
    fi
    num_hashes="$(sqlite3 "$filedb" "SELECT COUNT(id) FROM hashes")"
    [[ "$num_hashes" ]] || num_hashes=0
    if [[ "$num_hashes" -gt 0 ]]  ; then
        log "ERROR: $num_hashes directories already exist in the DB.  A new" \
            "scan makes no sense"
        exit 4
    fi
    log "Scanning directories and writing to db"
    (
        # TODO: if this is a rootfs, then don't add /proc or /sys
        # We must get the relative mountpoint here
        printf "BEGIN TRANSACTION;\n"
        find . \
            -mount \
            -type d \
            -printf \
                "INSERT INTO directories (
                    size, ctime, mtime, uid, gid, perm, inode, nlinks, device,
                    timestamp, name)
                VALUES (
                    %s, '%CY-%Cm-%Cd %CH:%CM:%CS', '%TY-%Tm-%Td %TH:%TM:%TS',
                    %U, %G, %m, %i, %n, %D, DATETIME('now'),
                    TAG_DIRNAME: '%p'
                );\n" \
        | sed "/^[ \t]*TAG_DIRNAME:\ '/s/'/''/g;s/''$/'/;s/[ \t]*TAG_DIRNAME:\ ''/'/"
        printf "COMMIT;\n"
    ) \
    | tee "$filedb".dirs.sql \
    | sqlite3 "$filedb"
    log_header "Directory Scan Complete.  num_directories: "
    num_directories="$(sqlite3 "$filedb" "SELECT COUNT(id) FROM directories")"
    log_append_line "$num_directories \n"
    [[ -f "$filedb".dirs.sql ]]   && xz "$filedb".dirs.sql &
    if [[ "$num_directories" -le 0 ]] ; then
            log "ERROR: no directories in db $filedb"
        exit 5
    fi
}
