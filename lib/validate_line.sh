#!/bin/sh
# Bryant Hansen

# Validate the line to be executed
# This line is a bash shell command to execute a hard link
# The source link is to replace the destination link
# The pointer to the destination inode is removed
# If it's the last pointer to that inode, the inode will be deleted and the
# duplicate space freed
# Steps:
#   Parse the line into it's 1)command, 2)source, and 3)dest arguments
#   Validate the source and the dest
#   Return a code to indicate whether the line should be executed

# Decide if this should be set here or if it should rely on inheritance from
# the calling script
set -e

validate_line() {
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/are_linked.sh
    . "$LIB_DIR"/human_readable.sh

    INVALID=1
    OK=0
    # TODO: atomic md5 file comparison & hard link
    if [[ "$l" != "$1" ]] ; then
        log "ERROR: line cannot be passed as parameter!"
        log "  line: $l"
        log "  param: $1"
        log "DO NOT EXECUTE LINE"
        return $INVALID
    fi
    l_parse1="${l#ln*\'}"
    l_parse2="${l_parse1%\' \;}"
    l_parse3="${l_parse2//\'\'\'/\'}"
    l1="${l_parse3%\' \'*}"
    l2="${l_parse3#*\' \'}"

    if [[ ! -f "$l1" ]] ; then
        log "ERROR: arg1 $l1 doesn't exist"
        log "  line=$l"
        log "DO NOT EXECUTE LINE"
        return $INVALID
    fi

    if [[ ! -f "$l2" ]] ; then
        log "ERROR: arg2 $l2 doesn't exist"
        log "  line=$l"
        log "DO NOT EXECUTE LINE"
        return $INVALID
    fi

    rm -f ".tmp.*"
    tf="$(mktemp ".tmp.XXXXXXXX")"
    ln -f "$l1" "$tf"
    if ! areLinked "$l1" "$tf" ; then
        log "ERROR: areLinked cannot detect a valid link: 1='${l1}' 2='${tf}'"
        rm "$tf"
        return $INVALID
    fi
    rm "$tf"

    # areLinked is currently broken, but not detected
    if areLinked "$l1" "$l2" ; then
        log "Files are already linked:"
        log "  $l1"
        log "  $l2"
        return $INVALID
    else
        md51="$(md5sum "$l1" | cut -f 1 -d ' ')"
        md52="$(md5sum "$l2" | cut -f 1 -d ' ')"
        if [[ "$md51" != "$md52" ]] ; then
            log "ERROR: file md5s do not match:"
            log "  $md51  $l1"
            log "  $md52  $l2"
            log "DO NOT EXECUTE LINE"
            return $INVALID
        fi
        links="$(stat --format="%h" "$l1")"
        if [[ "$links" == 1 ]] ; then
            savings="$(stat --format="%s" "$l2")"
            log "saving $(human_readable "$savings") removing inode" \
                "\"${l2}\""
        fi
        return $OK
    fi
    # should not reach this point.  If so, return error
    ERROR=2
    return $ERROR
}
