#!/bin/bash
# Bryant Hansen

# Interface to a statistics file, used to cache statistical results that
# require a long time to calculate.
# It facilitates the speedy generation of reports of relatively-massive filesystems

function write_stats() {
    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  stats_file"
    local DT=$(date "+%Y%m%d_%H%M%S")
    local stats_file="$1"

    [[ "$VERBOSE" ]] && echo "${DT}:${FUNCTION}: $*" >&2
    shift
    if [[ -f "$stats_file" ]] ; then
        echo "$(date +%s) $*" >> "$stats_file"
    else
        echo "${DT}:${FUNCTION}:ERROR: stats_file $stats_file not found" >&2
        echo "USAGE: ${USAGE}" >&2
        return 1
    fi
}

function read_stats() {
    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  stats_file  firstlast  field  key"
    local stats_file="$1"
    shift
    [[ "$VERBOSE" ]] && echo "${DT}:${FUNCTION}: $*" >&2
    if [[ -f "$stats_file" ]] ; then
        #local stats_file=./stats.log
        #    log_dirs=$(read_stats first value dirs)
        #         t1="$(read_stats first timestamp dirs)"
        local firstlast="$1"
        local field="$2"
        local key="$3"
        case $firstlast in
        first)
            match="$(grep -m 1 " $key " "$stats_file" || exit 0)"
            ;;
        last)
            match="$(grep " $key " "$stats_file" | tail -n 1 || exit 0)"
            ;;
        *)
            echo "${FUNCTION}:ERROR: unknown first/last param: ${firstlast}" >&2
            echo "USAGE: ${USAGE}" >&2
            return 2
            ;;
        esac
        if [[ "$match" ]] ; then
            #log "readstats: '${1}' match: '${match}'"
            ts="${match%% *}"
            key="${match#* }"
            key="${key% *}"
            value="${match##* }"
            case $field in
            timestamp)
                echo "$ts"
                ;;
            value)
                echo "$value"
                ;;
            *)
                echo "${FUNCTION}:ERROR: unknown field: ${field}"
                ;;
            esac
        fi
    fi
}
