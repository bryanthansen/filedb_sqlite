#!/bin/bash
# Bryant Hansen

# Helper function: is the database currently locked?


function is_db_unlocked() {

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh

    local db_status="$(sqlite3 "$db" .schema 2>&1 || echo "error_code=$?")"
    if [[ "$db_status" == "Error: database is locked" ]] ; then
        log_header ; printf "%20s: %s\n" "lock_status" "locked"
    else
        schema_lines="$(echo -e "$db_status" | wc -l)"
        if [[ "$schema_lines" -lt 10 ]] ; then
            log "ERROR: failed to fetch db schema.  Message:" \
                "${db_status}; consider DB locked"
            exit $stage
        else
            log_header
            printf "%20s: %s\n" "lock_status" "unlocked" >&2
            log_header
            printf "%20s: %s\n" "schema lines read" "$schema_lines" >&2
            return 0
        fi
    fi
    return 1
}
