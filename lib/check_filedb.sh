#!/bin/bash
# Bryant Hansen

# Description: check the filedb for basic integrity
#              has is beeen properly-initialized?

function check_filedb() {
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh

    local filedb="$1"

    # do we need to uncompress a previous
    if [[ ! -f "$filedb" ]] ; then
        if [[ -f "$filedb".gz  || -f "$filedb".xz ]] ; then
            log "Compressed logfile found, but not an uncompressed" \
                "version"
            flexcat "$filedb" 2>/dev/null > "$filedb"
        fi
    fi
    if [[ ! -f "$filedb" ]] ; then
        log "filedb '${filedb}' not found"
        log "ERROR: detect stage detected a DB, but we do not find" \
            "one here"
        return 2
    fi
    log "Checking if $filedb is unlocked and the tables are accessible"
    db_status="$(sqlite3 "$db" .schema 2>&1 || echo "error_code=$?")"
    if [[ "$db_status" == "Error: database is locked" ]] ; then
        log "ERROR: database is locked.  Can't continue"
        return 3
    else
        schema_lines="$(echo -e "$db_status" | wc -l)"
        if [[ "$schema_lines" -lt 10 ]] ; then
            log "ERROR: failed to fetch db schema.  Message:" \
                "$db_status"
            return 4
        else
            log "database schema number of lines: $schema_lines"
        fi
    fi
    log "Checking $filedb for valid tables"
    for table in directories files hashes ; do
        if ! sqlite3 "$filedb" | grep -q "CREATE TABLE $table (" ; then
            log "ERROR: $table table not found in $filedb"
            return 5
        fi
    done
    log "stage 1: database seems valid"
    return 0
}
