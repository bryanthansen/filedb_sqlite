#!/bin/sh
# Bryant Hansen

# Description: Make a backup copy of a file and truncate the original

# This is what we'd want to do if our file generation was interrupted and we
# want to resume.
# If it was interrupted at an invalid point (ie. not on a record boundary),
# then we want to truncate the information to the last valid point

#If a file has been interrupted, we may want to resume it from
# a certain point.

# TODO: don't truncate if the remaining lines are only whitespace
# TODO: create tests for this functionality

function backup_file_and_truncate_original() {
    set -e
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/last_valid_line.sh

    log "TEST EXIT"
    exit 100

    local file="$1"
    local last_valid_line_num="$2"
    local num_lines="$3"

    # If the last valid line is not the last line, we
    # want to replace the original file with only the
    # valid lines
    log "The last valid line in the shard is" \
        "line ${last_valid_line_num} of ${num_lines}: " \
        "'${last_valid_line}'"
    log "Invalid lines exist.  $last_valid_line_num of $num_lines are" \
        "valid in the file.  Truncate remaining"

    shard_filename="$(basename "$file")"
    shard_dirname="$(dirname "$file")"
    DT=$(date +%Y%m%d_%H%M%S)
    backup_dir="$shard_dirname"/.backup_shard_$DT
    mkdir -p "$backup_dir"
    log "backing up the last shard file $file to " \
        "$backup_dir"
    mv "$file" "$backup_dir"/
    backup_file="$backup_dir"/"$shard_filename"
    log "writing the first $last_valid_line_num lines to the " \
        "updated last_shard_file"
    flexcat "$backup_file" \
    | head -n $last_valid_line_num \
    | gzip \
    > "${file%.[gx]z}".gz

    if [[ "${file%.[gx]z}".gz != "$file" ]] ; then
        log "INFO: truncated and recompressed file" \
            "$file to ${file%.[gx]z}.gz"
        file="${file%.[gx]z}".gz
    fi

    log "Verify the fix of $file"

    log_header "Number of lines: "
    num_lines="$(flexcat "$file" 2>/dev/null | wc -l)"
    log_append_line "${num_lines}\n"

    log_header "Last valid line: "
    last_valid_line="$(flexcat "$file" 2>/dev/null \
                          | grep -n ";[ \t]*$" \
                          | tail -n 1
                      )"
    last_valid_line_num="${last_valid_line%%:*}"
    log_append_line "${last_valid_line_num}\n"

    if [[ "$last_valid_line_num" -le 0 ]] ; then
        log "ERROR: the last valid line number of file" \
            "$file not found"
        exit 39
    fi
    last_valid_line="${last_valid_line#*:}"
    log "After truncating, the last valid line in log file $file is" \
        "line ${last_valid_line_num}:" \
        "'${last_valid_line}'"
    log_append_line "${last_valid_line}\n"
    if [[ "$last_valid_line" != "$num_lines" ]] ; then
        log "ERROR: we were not able to truncate the file" \
            "$file to correct the syntax. "
            "num_lines=${num_lines}, last_valid_line=${last_valid_line}"
        exit 40
    fi
    echo "$last_valid_line"
    return 0
}

