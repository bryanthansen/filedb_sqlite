#!/bin/sh
# Bryant Hansen

# @brief Intended to be a function-based implementation to create an
#        SQLite-based filesystem database of the current directory tree

function timestamp() {
    db="$1"
    key="$2"
    value="$3"
    sqlite3 \
        "$db" \
        "BEGIN TRANSACTION;
         INSERT INTO statistics (key, value, timestamp)
         VALUES ('$key', '$value', (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')));
         COMMIT;
        "
}
