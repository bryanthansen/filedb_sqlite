#!/bin/sh
# Bryant Hansen

# @brief Perform parallel hashing and write SQL records

# Pipeline-compatible: Read from stdin, Write to stdout

# Stdin Format:
#   - Carriage-return-delimited inode/filename combinations
#   - Line starts with an inode number followed by a space
#   - The second argument is a filename
# TODO: determine how I am preserving whitespace with this operation

# Stdout Format:
#   - SQL statements
#   - Containing hashes records containing an inode and a hash
#   - Starting with a BEGIN TRANSACTION and ending with a COMMIT;

function hash_inodes() {
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh

    num_procs=3
    [[ "$NUM_PROCS" ]] && num_procs="$NUM_PROCS"
    local filedb="$1"

    if [[ "$num_procs" -gt 1 ]] ; then
        log "Performing parallel, chunked hash operation with" \
            "$num_procs threads"
        # complicated
        # Point is: don't add an SQL record if we've failed to hash
        cmd=' \
              md5="$(md5sum "${0#* }" | cut -f 1 -d " ")" || unset md5; \
              [[ \"$md5\" ]] && echo \
                  "INSERT INTO hashes ( inode, md5, timestamp ) \
                   VALUES ( ${0%% *}, \"${md5}\", DATETIME(\"now\") );"
        '

        # TODO: determine how to record stderr

        # FIXME: Synology Bash does not use the -c option the same way
        #        This code is broken on Synology

        # TODO: Fix parallel operation on Synology

        tr '\n' '\0' \
        | xargs -0 -n1 -P$num_procs bash -c "$cmd"
        log "Parallel hash operation complete."
    else
        log "Performing single-threaded, chunked hash operations"
        unset not_found
        while read inode filename ; do
            if [[ -f "$filename" ]] ; then
                md5="$(md5sum "${filename}" | cut -f 1 -d ' ')"
                if [[ "$md5" ]] ; then
                    echo "INSERT INTO hashes ( inode, md5, timestamp )" \
                         "VALUES ( ${inode}, '${md5}', DATETIME(\"now\") );"
                else
                    log "Failed to get md5 for file '${filename}'"
                fi
            else
                log "File not found: '${filename}'"
            fi
        done
        log "Single-threaded hash operation complete"
    fi
}
