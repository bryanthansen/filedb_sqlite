#!/bin/sh
# Bryant Hansen

# @brief Perform parallel hashing and write to DB in a single, atomic
#        transaction when finished

# For resuming, the db_inodes_to_hash function returns only inodes which have
# not yet been hashed

# Consider changing inode to primary key if we don't need to support
# time-variant data (ie. updates), which would mean a living database, rather
# than a static stapshot

function inode_hashes_to_db() {
    local FUNCTION=${FUNCNAME[0]}

    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/db_inodes_to_hash.sh
    . "$LIB_DIR"/hash_inodes.sh

    . "$SQL_DIR"/query_num_inodes_hashed.sql

    log_label "args" "$*"

    filedb="$1"
    sql_outfile="$filedb".hashes.sql

    log_label "filedb"      "$filedb"
    log_label "sql_outfile" "$(basename "$sql_outfile")"

    # Are we resuming an operation?
    # If the SQL logfile exists, we can assume so
    # If the SQL logfile does not exist, but a compressed version does,
    # then uncompress the compressed version and append to it
    # If neither the compressed nor uncompressed versions exist,
    # then start a new SQL log with a BEGIN TRANSACTION statement

    if [[ ! -f "$sql_outfile" ]] ; then
        if [[ -f "$sql_outfile".gz || -f "$sql_outfile".xz ]] ; then
            log "Restoring $(basename "$sql_outfile") from compressed archive"
            flexcat "$sql_outfile" 2>/dev/null \
            | grep -v "^COMMIT" \
            > "$sql_outfile"
            if ! cat "$sql_outfile" 2>/dev/null \
                | head -n 1 \
                | grep -q "BEGIN TRANSACTION;"
            then
                log "ERROR: restored file does not start with transaction"
                exit 92
            fi
        fi
    fi

    # If the file already exist and begins with a "BEGIN TRANSACTION", then
    # we are appending to this file
    if cat "$sql_outfile" 2>/dev/null \
         | head -n 1 \
         | grep -q "BEGIN TRANSACTION;"
    then
        log "Resuming file $sql_outfile"

        log_label "Number of Lines"
        num_sql_hash_lines="$(cat "$sql_outfile" \
                              | grep -v "^BEGIN\|^COMMIT" \
                              | wc -l)"
        log_append_line "${num_sql_hash_lines}\n"
        [[ "$num_sql_hash_lines" ]] || num_sql_hash_lines=0
        if [[ "$num_sql_hash_lines" -gt 0 ]] ; then
            log_label "Last Line"
            # get the last inode that was hashed
            last_line="$(cat "$sql_outfile" \
                        | tail \
                        | grep "\;[ \t]*$" \
                        | tail -n 1
                        )"
            log_append_line "${last_line}\n"

            # We need to import these files, but we don't want to import them a
            # second time
            # Flush first and then import?
            # Check that we're importing more lines that we're flushing?
            log_label "hash records in DB"
            num_hashes_in_db="$(sqlite3 \
                              "$filedb" \
                              "$sql_query_num_inodes_hashed"
                        )"
            log_append_line "${num_hashes_in_db}\n"
            [[ "$num_hashes_in_db" ]] || num_hashes_in_db=0
            if [[ "$num_hashes_in_db" -gt 0 ]] ; then
                if [[ "$num_sql_hash_lines" -gt "$num_hashes_in_db" ]] ; then
                    log "Deleting the original $num_hashes_in_db hash" \
                        "records from the DB to avoid duplicate records"
                    sqlite3 "$filedb" "DELETE FROM hashes;"
                    sqlite3 "$filedb" "VACUUM;"
                    log "Importing hashes to DB"
                    # Consider sanity-checking sql_outfile here
                    (
                        cat "$sql_outfile"
                        echo "COMMIT;"
                    ) | sqlite3 "$filedb"
                    log "Hash DB import complete."
                    log_label "hash records in DB"
                    num_hashes_in_db="$(sqlite3 \
                                      "$filedb" \
                                      "$sql_query_num_inodes_hashed"
                                )"
                    log_append_line "${num_hashes_in_db}\n"
                    [[ "$num_hashes_in_db" ]] || num_hashes_in_db=0
                    if [[ "$num_hashes_in_db" -le 0 ]] ; then
                        log "ERROR: no hashes logged to db $filedb"
                        exit 94
                    fi
                elif [[ "$num_sql_hash_lines" -eq "$num_hashes_in_db" ]] ; then
                    log "There is exactly the same number of records in the" \
                        "DB as in the SQL log file: $num_sql_hash_lines"
                    log "Assume the SQL logfile has already been imported"
                else
                    log "ERROR: there are more hashes in the DB than the log" \
                        "file.  Sanity check before replacing the DB with" \
                        "logfile contents."
                    exit 93
                fi
            fi
        fi
    fi
    if [[ -f "$sql_outfile" ]] ; then
        mv "$sql_outfile" "$sql_outfile".$(date +%Y%m%d_%H%M%S)
    fi
    printf "BEGIN TRANSACTION;\n" > "$sql_outfile"
    db_inodes_to_hash "$filedb" "$sql_outfile" \
    | hash_inodes \
    >> "$sql_outfile"
    printf "COMMIT;\n" >> "$sql_outfile"

    log "Hashing complete."

    # TODO: This reporting belongs in a separate function
    log_label "Number of Hash Lines"
    num_lines="$(cat "$sql_outfile" | wc -l)"
    log_append_line  "$num_lines lines (incl. BEGIN and COMMIT)\n"

    log "Importing hashes to DB"
    cat "$sql_outfile" | sqlite3 "$filedb"
    log "Hash DB import complete."

    log "Compressing SQL records"
    [[ -f "$sql_outfile" ]] && xz "$sql_outfile" &
    log "SQL file compression complete"

    # Note: this query required 1 hour and 6 minutes on an hashes
    #       table containing 6 million unique md5's
    log_label "hash records in DB"
    num_hashes="$(sqlite3 \
                      "$filedb" \
                      "SELECT COUNT(id) FROM hashes
                       WHERE md5 IS NOT NULL;"
                 )"
    log_append_line "${num_hashes}\n"
    if [[ "$num_hashes" -le 0 ]] ; then
        log "ERROR: no hashes logged to db $filedb"
        exit 4
    fi
}
