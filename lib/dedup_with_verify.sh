#!/bin/sh
# Bryant Hansen

# @brief Intended to be a function-based implementation to safely execute a
#        deduplication report which was created in the previous step

function dedup_with_verify() {

    set -e

    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  dbdir"

    printf "${ME}: SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/get_dbdir.sh
    . "$LIB_DIR"/flexcat.sh
    . "$LIB_DIR"/validate_line.sh
    . "$SQL_DIR"/query_duplicates.sql

    # Process Args
    while [[ "${1:0:1}" == '-' ]] ; do
        case "$1" in 
        "--already-linked-ok"|"-l")
            ALREADY_LINKED_IS_OK=1
            shift
            ;;
        "--verbose"|"-v")
            VERBOSE=1
            shift
            ;;
        "--")
            shift
            break
            ;;
        *)
            echo "$ME ERROR: unknown option: $1  abort." >&2
            return 4
            ;;
        esac
    done
    dbdir="$1"
    if [[ "$2" ]] ; then
        duplicate_report="$2"
    else
        duplicate_report="$dbdir"/duplicate_report.sh
        log "WARNING: duplicate_report not specified on command line;" \
            "defaulting to $duplicate_report"
    fi

    DT=$(date +%Y%m%d_%H%M%S)
    EXECUTE_LOG="./${dbdir}/execute_${DT}.log"
    log "Starting execution of ${duplicate_report}, logging to ${EXECUTE_LOG}"
    (
        df -h .
        df .
        num_lines="$(flexcat "$duplicate_report" | wc -l)"
        if [[ "$num_lines" -lt 1 ]] ; then
            log "ERROR: failed to read any lines in the duplicate_report" \
                "$duplicate_report"
            exit 5
        fi
        log "$num_lines hard link statements to be executed"
        available1="$(df --output=avail . | sed 1d)"
        if [[ "$NO_VERIFY" ]] ; then
            log "WARNING: executing deduplication without verification"
            flexcat "$duplicate_report"
        else
            line_num=0
            flexcat "$duplicate_report" \
            | while read l ; do
                let line_num=line_num+1
                if validate_line "$l" ; then
                    # hmmm...replace 3 single-quotes with a
                    # single-quote,escaped-quote,single-quote
                    # looks suspicious; deserves testing
                    echo "${l//\'\'\'/\'\\\'\'}"
                else
                    #log "ERROR: validate_line '${l}' failed; aborting"
                    #exit 3
                    log "$(basename "${duplicate_report}"):" \
                        "Line #$line_num is not valid"
                fi
                local update_period=1000
                if ! let line_num%update_period ; then
                    let pcnt_x_10=line_num*1000/num_lines || pcnt_x_10=0
                    [[ "${#pcnt_x_10}" == 1 ]] && pcnt_x_10=0$pcnt_x_10
                    dpcnt=${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}
                    log "Executing Line $line_num of $num_lines" \
                        "(${dpcnt}%% complete)"
                fi
            done
        fi \
        | /bin/bash
        df -h .
        df .
        available2="$(df --output=avail . | sed 1d)"
        savings="$(expr $available2 - $available1)"
        log "increase in disk availability since the start of the operation:" \
            "$savings 1k blocks"
    ) 2>&1 \
    | sed "s/^/\#\ /" \
    | tee -a "$EXECUTE_LOG" >&2

    log "deduplication complete; compressing logs"
    xz "$EXECUTE_LOG"
}
