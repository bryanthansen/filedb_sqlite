#!/bin/sh
# Bryant Hansen


function get_dbdir() {
    local FUNCTION=${FUNCNAME[0]}
    FILEDB_NAME=filedb.sqlite

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/get_latest_dbdir.sh

    if [[ "$1" ]] ; then
        if [[ -f "$1" ]] ; then
            log "${FUNCTION}:INFO: dbdir not specified; the current dir contains a filedb " \
                "${FILEDB_NAME}.  Using the curdir as dbdir and changing current " \
                "to parent"
            dbdir="$(dirname "$1")"
        elif [[ -d "$1" ]] ; then
            dbdir="$1"
        else
            log "ERROR: $1 does not exist as a file or directory"
            return 4
        fi
        dbdir_name="$(basename "$(readlink -f "$dbdir")")"
        if [[ "$dbdir_name" && "${dbdir_name%.filedb_*}" ]] ; then
            log "${FUNCTION}: ERROR: dbdir does not start with the required prefix .filedb_ (dbdir_name=${dbdir_name})"
            return 5
        fi
    else
        # No args; we need to guess the intent
        cur_dir_name="$(basename "$(readlink -f .)")"
        if [[ ! "${cur_dir_name%.filedb_*}" ]] ; then
            dbdir=.
        else
            # we not in a dbdir and one has not been specified
            # let's look for one
            log "${FUNCTION}:ERROR:No dbdir specified; guessing..."
            dbdir="$(get_latest_dbdir || echo "")"
            ret=$?
            if [[ "$ret" != 0 ]] ; then
                log "${FUNCTION}:WARNING: get_latest_dbdir returned code $ret"
            fi
        fi
    fi
    if [[ ! -d "$dbdir" ]] ; then
        if [[ "$dbdir" ]] ; then
            log "${FUNCTION}:ERROR: dbdir not valid: '${dbdir}'"
        else
            log "${FUNCTION}:ERROR: no valid dbdir found"
        fi
        return 6
    fi
    echo "$dbdir"
}
