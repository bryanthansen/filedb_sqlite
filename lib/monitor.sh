#!/bin/bash
# Bryant Hansen

# Description: monitor an operation-in-progress

# TODO: for hashing stage, get the total number of hashes required for a progress % estimation
# TODO: report scan rates

function monitor() {
    if which watch > /dev/null 2>&1 ; then
        watch -n 5 "
                f=filedb.sqlite.hashes.sql
                wc -l \$f
                tail -n 5 \$f | sed 's/\ \+/\ /g'
                ps auwx | grep ' md5sum' | grep -v grep
                dmesg | tail -n 6
                echo \"CPU temp=\$(cat /sys/class/thermal/thermal_zone0/temp)  GPU \$(vcgencmd measure_temp)\"
                for f in /sys/class/thermal/thermal_zone*/temp ; do echo \"\$f \$(cat \$f)\" ; done
        "
    else
        echo "# No watch functionality; looping over stats for X iterations with a period of Y"
        for n in `seq 1 5` ; do
            echo -e "\n# Iter $n"
            f=filedb.sqlite.hashes.sql
            wc -l $f
            tail -n 5 $f | sed "s/\ \+/\ /g"
            ps auwx | grep " md5sum" | grep -v grep
            dmesg | tail -n 6
            echo "CPU temp=$(cat /sys/class/thermal/thermal_zone0/temp)  GPU $(vcgencmd measure_temp)"
            sleep 10
        done
    fi
}
