#!/bin/sh
# Bryant Hansen

# @brief Intended to be a function-based implementation to create an
#        SQLite-based filesystem database of the current directory tree

function create_fdb() {

    FILEDB_NAME=filedb.sqlite

    local FUNCTION=${FUNCNAME[0]}
    local DT=$(date "+%Y%m%d_%H%M%S")

    PIDFILE=./create_fdb.pid
    echo "$$" > "$PIDFILE"

    # Process Args
    if [[ "$1" == "-v" ]] ; then
        VERBOSE=1
        shift
    fi

    printf "${DT}:${ME}:${FUNCTION}: ARGS: $* \n" >&2
    printf "${DT}:${ME}:${FUNCTION}: SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR\n" >&2

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/timestamp.sh
    . "$LIB_DIR"/dirs_to_db.sh
    . "$LIB_DIR"/files_to_db.sh
    . "$SQL_DIR"/create_db.sql

    # set directory locations
    dbdir=./.filedb_$(hostname)_$DT
    filedb="${dbdir}/${FILEDB_NAME}"
    absdir="$(readlink -f "$dbdir")"

    log "Creating filedb at $filedb"
    mkdir -p "$dbdir"
    echo -e "$create_db_sql" | sqlite3 "$filedb"
    timestamp "$filedb" "stage1"

    device="$(df . --output=source | sed 1d)"
    mountpoint="$(df . --output=target | sed 1d)"
    mp_rel_dir="$(dirname "${absdir#${mountpoint}}")"
    [[ "${mp_rel_dir:0:1}" == / ]] && mp_rel_dir="${mp_rel_dir#/}"
    [[ "$mp_rel_dir" ]] || mp_rel_dir=.

    log "Directory Tree:"
    log_label "PWD"        "$PWD"
    log_label "DEVICE"     "${device}"
    log_label "MOUNTPOINT" "${mountpoint}"
    log_label "mp_rel_dir" "${mp_rel_dir}"

    timestamp "$filedb" "stage2"    ; dirs_to_db "$filedb"
    timestamp "$filedb" "stage3"    ; files_to_db "$filedb"
    timestamp "$filedb" "end_time"

    rm "$PIDFILE"
}
