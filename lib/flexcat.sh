#!/bin/sh
# Bryant Hansen

# Description: this function is just a wrapper for 'cat' which will call zcat
#              with the compressed version
# of the same file if the
# uncompressed version doesn't exist
flexcat() {
    #printf  "~ flexcat $* ~ " >&2
    while [[ "$1" ]] ; do
        if [[ !    "${1/*.gz/}" ]] ; then
            echo -n "flexcat (zcat ${1}) " >&2
            zcat   "$1"
        elif [[ !  "${1/*.bz/}" ]] ; then
            echo -n "flexcat (bzcat ${1}) " >&2
            bzcat  "$1"
        elif [[ !  "${1/*.xz/}" ]] ; then
            echo -n "flexcat (xzcat ${1}) " >&2
            xzcat  "$1"
        elif [[ -f "$1" ]] ; then
            # use the uncompressed version if it exists
            cat    "$1"
        elif [[ -f "$1".gz ]] ; then
            # next priorities (in order): use the .gz, .bz, .xz
            echo -n "flexcat (zcat ${1}.gz) " >&2
            zcat   "$1".gz
        elif [[ -f "$1".bz ]] ; then
            echo -n "flexcat (bzcat ${1}.bz) " >&2
            bzcat  "$1".bz
        elif [[ -f "$1".xz ]] ; then
            echo -n "flexcat (xzcat ${1}.xz) " >&2
            xzcat  "$1".xz
        else
            return -1
        fi
        shift
    done
    return 0
}
