#!/bin/sh
# Bryant Hansen

# Description: Check if the 2 files to be linked are already

function areLinked() {

    USAGE="areLinked  file1  file2"

    SAME=0
    DIFFERENT=1
    ERROR=2

    i1="$(stat --format="%i" "$1")" 2>/dev/null
    i2="$(stat --format="%i" "$2")" 2>/dev/null

    # do 1 retry, sometimes there's an error when analyzing live files
    if [[ ! "$i1" ]] ; then
        log "inode fetch failed.  retry inode fetch for file $f1"
        sleep 1 && i1="$(stat --format="%i" "${f1//\"/}")"
        if [[ ! "$i1" ]] ; then
            log "Double failure when trying to fetch inode for file $f1"
        fi
    fi
    if [[ ! "$i2" ]] ; then
        log "inode fetch failed.  retry inode fetch for file $f2"
        sleep 1 && i2="$(stat --format="%i" "${f2//\"/}")"
        if [[ ! "$i2" ]] ; then
            log "Double failure when trying to fetch inode for file $f2"
        fi
    fi

    if [[ "$i1" ]] && [[ "$i2" ]] ; then
        if [[ "$i1" == "$i2" ]] ; then
#            log "# The files are linked"
            return $SAME
        else
#            log "# The files are NOT linked"
            return $DIFFERENT
        fi
    else
        [[ ! "$i1" ]] && log "the inode for ${f1} is null!"
        [[ ! "$i2" ]] && log "the inode for ${f2} is null!"
        return $ERROR
    fi
}
