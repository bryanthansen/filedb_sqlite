#!/bin/bash
# Bryant Hansen

# Get the maximum directory id in the directory table

function db_last_scan_dir() {
    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  filedb"

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh

    log_header
    printf "%20s: " "DB Last Dir Scanned" >&2
    # res="$(sqlite3 "$filedb" "SELECT MAX(dirid) FROM files)"
    local last_dirid_scanned="$(sqlite3 \
                        -separator ' ' \
                        "$1" \
                        "SELECT MAX(dirid)
                        FROM files
                        WHERE name NOT LIKE '%' || CHAR(10) || '%' ;"
                    )"
    log_append_line "${last_dirid_scanned}\n"
    [[ "$last_dirid_scanned" ]] || last_dirid_scanned=0
    echo "$last_dirid_scanned"
    [[ "$last_dirid_scanned" -gt 0 ]] && return 0 || return 1
}
