#!/bin/bash
# Bryant Hansen

# Description: be able to resume huge, multi-day processing jobs

# Stages:
#   1) create database
#   2) scan directories
#   3) scan files
#   4) hash inodes
#   5) produce dedup report
#   6) execute dedup report
#   7) complete


function resume() {
    local FUNCTION=${FUNCNAME[0]}

    log "Enter resume: PWD: $PWD"
    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/timestamp.sh
    . "$LIB_DIR"/get_dbdir.sh
    . "$LIB_DIR"/detect_stage.sh
    . "$LIB_DIR"/check_filedb.sh
    . "$LIB_DIR"/files_to_db.sh
    . "$LIB_DIR"/inode_hashes_to_db.sh

    unset filedb
    dbdir="$(get_dbdir "$*" || echo -n "")"
    if [[ ! -d "$dbdir" ]] ; then
        log "WARNING: no dbdir found: '${dbdir}'"
        unset dbdir
    else
        if [[ "$(readlink -f .)" == "$(readlink -f "$dbdir")" ]] ; then
            # We are not in the same context that the filedb was created in
            # When the filedb was created, the current directory is our parent
            # directory
            # Change to the parent and update the dbdir
            dbdir="./$(basename "$PWD")"
            #dbdir="$PWD"
            cd ..
        fi
        filedb="$dbdir"/filedb.sqlite
        log "filedb: ${filedb}"
        log "PWD: ${PWD}"
    fi

    current_stage="$(detect_stage "$dbdir")"
    LAST_STAGE=7

    log_label "Current Stage" "$current_stage"

    for stage in $(seq $current_stage $LAST_STAGE) ; do
        case $stage in
        0|INVALID)
            log "WARNING: stage 0: no valid filedb info found" \
                "(dbdir=${dbdir}, filedb=${filedb}). "
            echo >&2
            log "If you are expecting to resume something, you can't. " \
                "Use 'fdb.sh create' and start over"
            exit 10
            ;;
        1|CREATE_DB)
            if ! check_filedb "$filedb" ; then
                log "ERROR: filedb $filedb not valid"
                exit $stage
            fi
            ;;
        2|CREATE_DIR_TABLE)
            log "Stage ${stage}:CREATE_DIR_TABLE"
            dirs_to_db "$filedb" ; ret=$?
            if [[ "$ret" -ne 0 ]] ; then
                log "ERROR: dirs_to_db failed, code $ret"
                exit $stage
            fi
            ;;
        3|CREATE_FILE_TABLE)
            log "stage ${stage}:CREATE_FILE_TABLE (PWD: '$PWD'  filedb: '$filedb')"
            files_to_db "$filedb" ; ret=$?
            if [[ "$ret" -ne 0 ]] ; then
                log "ERROR: files_to_db failed, code $ret"
                exit $stage
            fi
            ;;
        4|CREATE_FILE_HASH_TABLE)
            log_label "stage" "$stage:CREATE_FILE_HASH_TABLE"
            inode_hashes_to_db  "$filedb" ; ret=$?
            if [[ "$ret" -ne 0 ]] ; then
                log "inode_hashes_to_db failed, code $ret"
                exit $stage
            fi
            ;;
        5|DEDUP)
            log "stage $stage DEDUP.  Produce report: " \
                "'fdb.sh dedup > \$dbdir/duplicate_report.sh'"
            exit 0
            ;;
        6|EXECUTE)
            log "stage $stage EXECUTE.  Execute report: " \
                "'fdb.sh execute \$dbdir'"
            exit 0
            ;;
        7|COMPLETE)
            log "stage $stage COMPLETE.  The entire operation is complete" \
                "in $PWD and logged to ${dbdir}.  Nothing to resume"
            exit 0
            ;;
        *)
            log "unknown stage: $stage"
            exit -1
            ;;
        esac
        timestamp "$filedb" stage $stage
    done

    return 0
}
