#!/bin/bash
# Bryant Hansen

# @brief Intended to be a function-based implementation to produce a deduplicate
#        report, based on information in an existing SQLite filesystem database

function dedup_report() {

    set -e
    local FUNCTION=${FUNCNAME[0]}
    printf "${ME}:${FUNCTION}: ARGS: $* \n" >&2

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/get_dbdir.sh
    . "$LIB_DIR"/timestamp.sh
    . "$LIB_DIR"/human_readable.sh

    . "$SQL_DIR"/file_report.sql
    . "$SQL_DIR"/query_duplicates.sql
    . "$SQL_DIR"/query_num_large_inodes_hashed.sql
    . "$SQL_DIR"/query_num_unique_hashes.sql

    dbdir="$(get_dbdir "$1")"
    log_label "dbdir" "$dbdir"

    FILEDB_NAME=filedb.sqlite
    filedb="$dbdir"/"$FILEDB_NAME"
    if [[ ! -f "$filedb" ]]  ; then
        log "ERROR: filedb '${filedb}' does not exist"
        exit 101
    fi
    log_label "filedb" "$(basename "$filedb") (exists)"

    db_size="$(stat --format=%s "$filedb")"
    db_size_hr="$(human_readable "$db_size")"
    log_label "DB Size" "$db_size bytes (${db_size_hr}B)"

    local DT=$(date "+%Y%m%d_%H%M%S")
    local outfile="${dbdir}/duplicate_report_${DT}.sh"
    log_label "Report" "$outfile"

    log_label "Hash Records in DB"
    num_hashes_in_db="$(sqlite3 "$filedb" \
                        "SELECT COUNT(id)
                         FROM hashes
                         WHERE md5 IS NOT NULL;"
    )"
    log_append_line "${num_hashes_in_db} "
    log_append_line "($(human_readable "$num_hashes_in_db"))\n"

    [[ "$num_hashes_in_db" ]] || num_hashes_in_db=0
    if [[ "$num_hashes_in_db" -le 0 ]] ; then
        log "ERROR: no hashes in DB.  Exiting"
        exit 3
    fi

    log_label "Num Inodes Hashed (>=8192)"
    num_inodes_hashed="$(
        sqlite3 "$filedb" "$sql_num_large_inodes_hashed"
    )"
    log_append_line "${num_inodes_hashed} "
    log_append_line "($(human_readable "$num_inodes_hashed"))\n"

    log_label "Num Unique Hashes (>=8192)"
    num_hashes="$(
        sqlite3 "$filedb" "$sql_num_unique_hashes"
    )"
    log_append_line "${num_hashes} $(human_readable "$num_hashes"))\n"

    let num_duplicates=num_inodes_hashed-num_hashes
    log_label "Num Dup Inodes (>=8192)"
    log_append_line "$num_duplicates ($(human_readable "$num_duplicates"))\n"

    [[ "$num_duplicates" ]] || num_duplicates=0
    if [[ "$num_duplicates" -le 0 ]] ; then
        log "No duplicates in DB.  Exiting"
        exit 4
    fi

    log_label "Num Links Expected" "TODO"

    unset shard
    if [[ "$num_duplicates" -gt 100000 ]] ; then
        shard=1
    fi

    # TODO: determine why the query source variable causes an error when
    #       displayed"
    #printf "${ME}:${FUNCTION}: about to print query:\n" >&2
    #printf "${ME}:${FUNCTION}: INFO sql_query_duplicates: \
    #                           '${sql_query_duplicates}'\n" >&2
    #printf "${ME}:${FUNCTION}: printed query\n" >&2

    # Now dump an executable report
    unset last_md5
    if [[ "$shard" ]] ; then
        log_label "Sharding" "enabled"
        for n in {0..16} ; do
            local md5_prefix="$(printf "%x" "$n")"
            let pcnt_x_10=n*1000/16 \
            || pcnt_x_10=0
            [[ "${#pcnt_x_10}" == 1 ]] && pcnt_x_10=0$pcnt_x_10
            dpcnt=${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}
            log_label "Hash Prefix" \
                      "${md5_prefix} ($n of 16) - ${dpcnt}%% complete"
            local sql="$(gen_query_duplicates "$md5_prefix")"
            sqlite3 -separator " " "$filedb" "$sql"
            if [[ -f "$outfile" ]] ; then
                log_label "Dedup Lines"
                dedup_lines="$(cat "$outfile" | wc -l)"
                #let pcnt_x_10=dedup_lines*1000/num_duplicates \
                #|| pcnt_x_10=0
                #[[ "${#pcnt_x_10}" == 1 ]] && pcnt_x_10=0$pcnt_x_10
                #dpcnt=${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}
                #log_append_line "${dedup_lines} of ${num_duplicates}" \
                #                "(${dpcnt}%%)\n"
                log_append_line "${dedup_lines}\n"
            fi
        done
    else
        log_label "Sharding" "disabled"
        # TODO: Announce error on null queries; sqlite3 does not
        sqlite3 -separator ' ' "$filedb" "$sql_query_duplicates"
        log_header "sql_query_duplicates: "
        echo -e "${sql_query_duplicates}" | tr '|' ' ' >&2
    fi \
    | while read md5 inode file ; do

        # write executable hard link statements to stdout

        # The first file will be the reference inode
        # Files with the same MD5 and another inode will be replaced with the
        # first

        # The force flag (-f) may be required to overwrite existing files
        # ln -f <first_file> <current_file>

        # A semicolon is added at the end to clarify end-of-statement and limit
        # the damage, in case of accidental carriage returns due to
        # programming errors (ie. colliding, out-of-sync streams)
        # We may be running in multi-process mode, so collision is a ligitimate
        # concern

        if [[ "$last_md5" == "$md5" ]] ; then
            if [[ "$first_inode" != "$inode" ]] ; then
                echo "ln -f '${first_file}' '${file}' ;"
            fi
        else
            # set the reference inode for this md5
            first_inode="$inode"
            first_file="$file"
        fi
        last_md5="$md5"
    done \
    | sed "
          # This mess replaces some highly-inefficient lines of bash shell code
          # in the loop

              # Strip leading and trailing single-quotes
              #file="${file:1:-1}"

              # Replace remaining single-quotes escaped single-quotes
              #file="${file//\'/\'\\\'\'}"

              # Then add the leading and trailing single-quotes at the end

          # The following SED looks messy, but there's a purpose for it.

          # It is a single shell function which processes a stream of data
          # No inefficient lines of shell code executing in a loop

          # Single-quotes must be escaped 2 levels deep, since the shell
          # processes the first level of escape characters
          # Therefore, there a chain of 4 escape characters to match a single
          # escape from stdin

          # Single-quotes do not need to be escaped when the SED command
          # is passed to sed wrapped in double-quotes

          s/'/'\\\\''/g;
          s/'\\\\'''\\\\''\ '\\\\'''\\\\''/'\ '/;
          s/^ln\ \-f\ '\\\\'''\\\\''/ln\ \-f\ '/;
          s/'\\\\'''\\\\''\ \;/'\ \;/
    " > "$outfile"

    if [[ ! -f "${dbdir}/duplicate_report.sh" ]] ; then
        ln -s "$(basename "$outfile")" "${dbdir}/duplicate_report.sh"
    fi

    timestamp "$filedb" "stage5"
    log exit
}
