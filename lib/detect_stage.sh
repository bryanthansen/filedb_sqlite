#!/bin/bash
# Bryant Hansen

# Description: detect the stage, based on the current trace of the database and
#              various log files

# Stages:
#   1) create database
#   2) scan directories
#   3) scan files
#   4) hash inodes
#   5) produce dedup report
#   6) execute dedup report
#   7) complete

# TODO: consider verifying if log files have records

function detect_stage() {
    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  dbdir"
    local stage=0
    local dbdir="$1"
    local filedb="$dbdir"/filedb.sqlite

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/flexcat.sh
    . "$LIB_DIR"/db_last_scan_dir.sh
    . "$LIB_DIR"/db_check_lock_status.sh

    if [[ ! -d "$dbdir" ]] ; then
        echo $stage
        return 0
    fi

    # Scan stages in reverse order, but show the code in forward order
    # Hopefully that makes logical sense somehow
    # We don't want to unnecessarily scan the early stages, but it's nice
    # to code stages in the proper sequence of the process
    for stage in $(seq 7 -1 1) ; do
        case $stage in
        1)
            log "filedb: $filedb"
            if [[ -f "$filedb" || -f "$filedb".gz  || -f "$filedb".xz ]] ; then
                stage=1
            else
                log "filedb does not exist in compressed or uncompressed" \
                    "forms.  stage=$stage"
                echo $stage
                return 0
            fi
            stage=1
            log_label "filedb exists"
            log_append_line "Stage $stage started\n"
            # TODO: test lock here
            if ! is_db_unlocked "$filedb" ; then
                log "DB is locked"
            fi
            ;;
        2)
            # Check for stage 2: scan directories
            local db_max_dirid="$(
                sqlite3 "$filedb" "SELECT MAX(id) FROM directories"
            )"
            [[ "$db_max_dirid" ]] || db_max_dirid=0
            #log_append_line "${db_max_dirid}\n"
            if [[ "$db_max_dirid" ]] ; then
                log "Dirs exist in DB"
                log_label "Stage $stage" "started"
                break
            else
                log "No directories exist in the db; stage 2 does not appear" \
                    "to be complete"
            fi
            log "Checking logfiles for directories"
            if ls -1 "$filedb".dirs.sql > /dev/null 2>&1\
            || ls -1 "$filedb".dirs.sql.[gx]z > /dev/null 2>&1 \
            || ls -1 "$filedb".files.dirs.[0-9]* > /dev/null 2>&1
            then
                log "Dir log found"
                log_label "Stage $stage" "started"
                break
            else
                log "No ${dbdir}/filedb.sqlite.dirs*.sql* found (PWD=$PWD)"
            fi
            ;;
        3)
            # Check for stage 3: scan files
            log "Checking logfiles for files"
            if ls -1 "$filedb".files.sql > /dev/null 2>&1 \
            || ls -1 "$filedb".files.[0-9]*.sql.[gx]z > /dev/null 2>&1
            then
                log "File log found"
                log_label "Stage $stage" "started"
                break
            else
                log "No file-scan logfiles found"
            fi

            log "Checking DB for last directory that has already been scanned"
            db_max_file_dirid="$(
                sqlite3 "$filedb" "SELECT MAX(dirid) FROM files"
            )"
            [[ "$db_max_file_dirid" ]] || db_max_file_dirid=0
            log_header
            printf "%20s: %s\n" "db_max_file_dirid" "$db_max_file_dirid" >&2
            [[ "$db_max_file_dirid" ]] || db_max_file_dirid=0
            if [[ "$db_max_file_dirid" -gt 0 ]] ; then
                if [[ "$db_max_file_dirid" == "$db_max_dirid" ]] ; then
                    # We have completed scanning directories for files and the
                    # db is updated
                    log "All directories already scanned for files;" \
                        "stage 3 is complete"
                    stage=5
                    break
                else
                    log_label "Dir Records in DB" "$db_max_dirid"
                    log_label "Dirs Scanned" "$db_max_file_dirid"

                    local remaining
                    let remaining=db_max_dirid-db_max_file_dirid
                    log_label "Remaining Dirs" "$remaining"
                    log_label "Stage $stage" "not complete"
                fi
            fi
            ;;
        4)
            log "WARNING: stage $stage does not exist"
            #exit $stage
            ;;
        5)
            dup_report="$dbdir"/duplicate_report.sh
            if ls -1 "$dbdir"/duplicate_report.sh* > /dev/null 2>&1 ; then
                log "duplicate_report.sh exists"
                log_label "Stage $stage" "started"
                break
            else
                log "Report does not exist"
                log_label "Stage $stage" "not yet started"
                unset dup_report
            fi
            ;;
        6)
            if ls -1 "$dbdir"/execute*.log > /dev/null 2>&1 ; then
                stage=6
                log "Execute logs found"
                log_label "Stage $stage" "started"
                break
            else
                if ls -1 "$dbdir"/execute*.log.[gx]z > /dev/null 2>&1 ; then
                    stage=6
                    log "compressed execute logs found.  Stage $stage started"
                    # TODO: Check if stage 6 is complete
                    #       Are the 2 files in the last hard link statement
                    #       hard-linked in the filesystem?
                    log_header "Last line of duplicate report: "
                    last_dedup_line="$(
                        flexcat "$dup_report" 2>/dev/null \
                        | tail \
                        | grep "^[ \t]*ln\ " \
                        | tail -n 1
                    )"
                    log_append_line "${last_dedup_line}\n"
                    log "TODO: check if that line has been executed already."
                else
                    log "No execute logs found"
                    log_label "Stage $stage" "not yet started"
                fi
            fi
            ;;
        7)
            log "TODO: check for completion here"
            ;;
        *)
            log "ERROR: unknown stage $stage"
            exit 2
        esac
    done

    echo "$stage"
}

