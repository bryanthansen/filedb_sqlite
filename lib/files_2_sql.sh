#!/bin/sh
# Bryant Hansen

# Description: scan all files in a directory tree and output SQL records

# Loop through every directory record in the DB and record all files in each
# Support the ability to resume an interrupted operation

# Requirements:
#   * Support the ability to resume an interrupted operation
#   * Output data to sharded files, since it can be a *huge* amount of
#     information

# Notes:
#   - consider a stdout option for small jobs or ones that specifically should
#     be pipelined
#   - pipelined jobs cannot be easily resumed, which is a problem if they need
#     to run for multiple days to complete

# Set this to do hashing in this function instead of a separate stage
# Advantage: the file scan-and-hash stage can be done in parallel on different
#            chunks/shards.  In addition, redundant operations must be performed
#DO_HASHING=1

function files_2_sql() {

    set -e

    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  filedb"
    local filedb="$1"
    local dir_start_id=0
    local shard_size=100000
    [[ "$SHARD_SIZE" ]] && shard_size="$SHARD_SIZE"

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/last_file_shard_id.sh
    . "$LIB_DIR"/log_last_dir_scan.sh

    local mountpoint="$(df . --output=target | sed 1d)"
    local basedir="$(readlink -f .)"
    local mp_rel_dir="$(dirname "${basedir#${mountpoint}}")"

    #log_header "Total dirs in DB: "
    log "sqlite3 '$filedb' 'SELECT MAX(id) FROM directories' (PWD: $PWD)"
    local db_max_dirid="$(sqlite3 "$filedb" "SELECT MAX(id) FROM directories")"
    [[ "$db_max_dirid" ]] || db_max_dirid=0
    #log_append_line "${db_max_dirid}"
    unset shard
    if [[ "$db_max_dirid" -gt "$shard_size" ]] ; then
        shard=1
        log_header
        printf "%20s: %s\n" "Shard Size" "$shard_size"
    else
        log "Sharding not enabled"
    fi
    if [[ "$db_max_dirid" -le 0 ]] ; then
        log "ERROR: db_max_dirid is $db_max_dirid in DB $filedb"
        exit 3
    fi

    dbdir="$(dirname "$filedb")"

    log "Checking last dir scanned in logfiles..."
    log_last_dir_scan=$(log_last_dir_scan "$dbdir")
    log_label "log_last_dir_scan" "$log_last_dir_scan"

    if [[ "$log_last_dir_scan" -eq "$db_max_dirid" ]] ; then
        # We have completed the filescan-to-SQL phase
        log "File scan complete."
        log_label "Dirs Scanned" "$db_max_dirid"
        return 0
    fi
    if [[ "$log_last_dir_scan" -gt "$db_max_dirid" ]] ; then
        log "ERROR: The max directory id $log_last_dir_scan in the" \
            "logs should not be greater than the max directory id in the" \
            "directories table $db_max_dirid"
        exit 41
    fi

    # if [[ "$log_last_dir_scan" -lt "$db_max_dirid" ]] ; then

    dir_start_id=$log_last_dir_scan

    log_header
    log_append_line "dir_start_id=${dir_start_id}, "
    log_append_line "remaining_dirs: "
    # Consider using a calculation here instead, unless there is a
    # possibility of holes in the ID sequence
    remaining_dirs="$(sqlite3 \
                        -separator ' ' \
                        "$filedb" \
                        "SELECT COUNT(id)
                        FROM directories
                        WHERE name NOT LIKE '%' || CHAR(10) || '%'
                        AND id > $dir_start_id ;"
                    )"
    log_append_line "${remaining_dirs}\n"

    iter="$(last_file_shard_id "$filedb")"
    log "Scanning files in the directory tree starting with directory" \
        "$dir_start_id with shard $iter"
    unset shard_start_time shard_end_time

    let num_iter=(db_max_dirid-dir_start_id)/shard_size
    for did in $(seq $dir_start_id $shard_size $db_max_dirid) ; do
        shard_start_time=$(date +%s)
        endid="$(expr ${did} + ${shard_size})"
        if [[ "$shard" ]] ; then
            let iter=iter+1
            log "scan directories block ${iter} of ${num_iter}:" \
                "${did} -> ${endid}"
            format_iter="$(printf "%06d" ${iter})"
            file_scan_shard="$filedb".files.${format_iter}.sql.gz
        else
            file_scan_shard="$filedb".files.sql.gz
        fi
        log "file_scan_shard=$(basename "$file_scan_shard")"
        if [[ -f "$file_scan_shard" ]] ; then
            log "ERROR: the file log already exists: $file_scan_shard"
            exit 7
        fi
        (
            printf "BEGIN TRANSACTION;\n"
            sqlite3 \
                -separator ' ' \
                "$filedb" \
                "SELECT id, name
                 FROM directories
                 WHERE name NOT LIKE '%' || CHAR(10) || '%'
                 AND id >= $did AND id < $endid ;" \
            | while read id dir ; do
                # If the directory is another mountpoint, then don't descend
                # into it
                if [[ ! -d "$dir" ]] ; then
                    log "WARNING: directory '${dir}' has disappeaered"
                else
                    mp="$(df "$dir" --output=target | sed 1d)"
                    if [[ "$mountpoint" != "$mp" ]] ; then
                        log "INFO: '${dir}' is a different mountpoint (${mp})" \
                            "than our root (${mp_rel_dir} on ${mountpoint}). " \
                            "Skipping directory scan"
                        continue
                    fi
                    DATE_STR="+%Y-%m-%dT%H%M%S%z"
                    DT="$(date "+%Y-%m-%dT%H:%M:%S")"
                    # Print all file parameters and do call a hash subcommend
                    # from a single pair of find options
                    # It requires massaging the output with SED to combine
                    # the hash into the query
                    # Hopefully all versions of find across distributions
                    # will do this.  This remains to be seen...
                    find "$dir" \
                      -maxdepth 1 \
                      -type f \
                      -printf \
                          "INSERT INTO files ( \
                              dirid, size, ctime, mtime, uid, gid, perm, \
                              inode, nlinks, timestamp, name \
                          )
                          VALUES ( \
                              ${id}, %s, '%CY-%Cm-%CdT%CH:%CM:%CS', \
                              '%TY-%Tm-%TdT%TH:%TM:%TS', %U, %G, %m, %i, %n, \
                              '${DT}',
                              TAG_FILENAME: '%P'
                          );
                          INSERT INTO hashes ( \
                            inode, md5, timestamp \
                          )
                          VALUES ( \
                              %i,
                          '" \
                      -exec md5sum "{}" \; \
                    | sed "
                        s/^[ \t]*// ;
                        /^[ \t]*'[0-9a-f]\{32\}\ /s/\ .*/'\,\n'${DT}'\ )\;\n/ ;
                        /^[ \t]*TAG_FILENAME:\ '/s/'/''/g ;
                        s/[ \t]*TAG_FILENAME:\ ''/'/ ;
                        s/''$/'/ ;
                    "
                    # NOTE: There seems to be a *significant* performance hit
                    #       when interleaving the SQL statements between 2
                    #       tables.
                    #       Either a 2-step import process should be
                    #       undertaken, preventing a direct pipeline to the
                    #       DB import process or
                    #       Try an SQL REINDEX after completion
                fi
            done
            printf "COMMIT;\n"
        ) \
        | gzip \
        >> "$file_scan_shard"
        log "File SQL output: $file_scan_shard"
        shard_end_time=$(date +%s)
        log "shard_end_time='$shard_end_time'   " \
            "shard_start_time='$shard_start_time'"
        elapsed=$(expr $shard_end_time - $shard_start_time) || elapsed=0
        log "shard elapsed time: $elapsed"
        if [[ "$elapsed" -gt 0 ]] ; then
            if [[ "$shard" ]] ; then
                let scan_rate=shard_size/elapsed
            else
                let scan_rate=db_max_dirid/elapsed
            fi
            log "file scan rate: $scan_rate directories per second"
        fi
    done

    # File scanning complete
    return 0
}
