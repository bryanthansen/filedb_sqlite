#!/bin/sh
# Bryant Hansen

# @brief Dump all inodes from the DB that are not yet hashed

# Requirements:
# - Dump to stdout for pipelining
# - Determine if chunking must be performed, due to potential out-of-memory
#   issues with large datasets on low-memory devices
# - Can resume a hash operation; make a starting query that
#   includes only inodes which have no associated hash


function db_inodes_to_hash() {
    local FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh

    . "$SQL_DIR"/query_num_inodes_to_hash.sql
    . "$SQL_DIR"/query_max_inode_to_hash.sql
    . "$SQL_DIR"/query_min_inode_to_hash.sql
    . "$SQL_DIR"/query_inodes_to_hash.sql
    . "$SQL_DIR"/query_num_inodes_hashed.sql

    #sql_query_num_inodes_hashed
    local filedb="$1"
    local sql_outfile="$2"
    INODE_CHUNK_SIZE=1000000

    # First do a sanity check
    max_file_id="$(sqlite3 "$filedb" "SELECT MAX(id) FROM files")"
    log_label "Max File ID" "$max_file_id"
    [[ "$max_file_id" ]] || max_file_id=0
    if [[ "$max_file_id" -le 0 ]] ; then
        log "ERROR: max files table id of $max_file_id in DB $filedb"
        exit 5
    fi

    log_label "Num Inodes"
    num_inodes="$(sqlite3 "$filedb" "SELECT COUNT(DISTINCT inode) FROM files")"
    log_append_line "${num_inodes}\n"
    [[ "$num_inodes" ]] || num_inodes=0
    if [[ "$num_inodes" -le 0 ]] ; then
        log "ERROR: the number of inodes $num_inodes in the DB is invalid"
        exit 6
    fi

    log_label "Num Inodes Hashed"
    num_inodes_hashed="$(sqlite3 "$filedb" "$sql_query_num_inodes_hashed")"
    log_append_line "${num_inodes_hashed}\n"

    log_label "Num Inodes to Hash"
    num_inodes_to_hash="$(sqlite3 "$filedb" "$sql_num_inodes_to_hash")"
    log_append_line "${num_inodes_to_hash}\n"

    unset chunk
    [[ "$num_inodes_to_hash" -le "$INODE_CHUNK_SIZE" ]] || chunk=1

    if [[ "$chunk" ]] ; then
        log_label "Chunk Size" "${INODE_CHUNK_SIZE} inode records\n"
        log "The number of inodes to hash is over our theshold for chunking"

        log_label "Max Inode to Hash"
        max_inode_to_hash="$(sqlite3 "$filedb" "$sql_query_max_inode_to_hash")"
        log_append_line "${max_inode_to_hash}\n"

        log_label "Min Inode to Hash"
        min_inode_to_hash="$(sqlite3 "$filedb" "$sql_query_min_inode_to_hash")"
        log_append_line "${min_inode_to_hash}\n"

        # Add a chunk to the end range, to be sure we
        let end_chunk=max_inode_to_hash+INODE_CHUNK_SIZE
        for start_inode in $(
            seq $min_inode_to_hash $INODE_CHUNK_SIZE $end_chunk
        ) ; do
            let end_inode=start_inode+INODE_CHUNK_SIZE
            let pcnt_x_10=start_inode*1000/max_inode_to_hash \
            || pcnt_x_10=0
            [[ "${#pcnt_x_10}" == 1 ]] && pcnt_x_10=0$pcnt_x_10
            dpcnt=${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}
            log "Hashing inodes $start_inode to $end_inode" \
                "of ${max_inode_to_hash} (${dpcnt}%% complete)"
            sqlite3 \
                -separator ' ' \
                "$filedb" \
                "$(query_inode_chunk_to_hash $start_inode $end_inode)"

            # count hash lines
            if [[ "$sql_outfile" && -f "$sql_outfile" ]] ; then
                log_label "Hashes Logged"
                hashes_logged="$(flexcat "$sql_outfile" 2>/dev/null | wc -l)"
                let pcnt_x_10=hashes_logged*1000/num_inodes_to_hash \
                || pcnt_x_10=0
                [[ "${#pcnt_x_10}" == 1 ]] && pcnt_x_10=0$pcnt_x_10
                dpcnt=${pcnt_x_10: 0: -1}.${pcnt_x_10: -1}
                log_append_line "${hashes_logged} of ${num_inodes_to_hash}" \
                                "(${dpcnt}%% commplete)\n"
            fi

        done
    else
        # This query fails with large sets on small devices
        # Both the ORDER BY and GROUP BY clauses cause a database
        # error when the sets are too big (ie. 70 million records on
        # a device with 1GB of RAM
        #    ERROR TEXT:
        #        Error: near line 3: database or disk is full
        sqlite3 -separator " " "$filedb" "$sql_query_inodes_to_hash"
    fi

}
