#!/bin/sh
# Bryant Hansen

# Description: import files into the DB

# This has been split into a 2-step process:
#   1) Scan all files in recorded directories and dump metadata as SQL records
#   2) Import SQL records into DB

# Requirements:
#   * Support the ability to resume an interrupted operation
#   * Output data to sharded files, since it can be a *huge* amount of
#     information
#   * Produce some minimal summary statistics on completion to verify the
#     success or failure of the operation

function files_to_db() {

    local FUNCTION=${FUNCNAME[0]}
    local USAGE="$FUNCTION  filedb"
    local filedb="$1"

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/files_2_sql.sh
    . "$LIB_DIR"/import_file_sql.sh

    if ! files_2_sql "$filedb" ; then
        log "ERROR: files_2_sql filed"
        exit 4
    fi
    if ! import_file_sql "$filedb" ; then
        log "ERROR: import_file_sql filed"
        exit 5
    fi
}
