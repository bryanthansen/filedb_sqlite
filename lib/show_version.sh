#!/bin/sh
# Bryant Hansen

# Description: show git version information

function show_version() {
    . "$LIB_DIR"/log.sh
    . "$LIB_DIR"/version.sh

    log_label "PWD"                  "$PWD"
    log_label "args"                 "'$*'"
    log_label "Git Branch"           "$GIT_BRANCH"
    log_label "Git Date"             "$GIT_COMMIT_DATE"
    log_label "Git Hash"             "${GIT_HASH:0:8}"
    log_label "Git Seq. Commit Num"  "$GIT_REVISION"
    if [[ "$GIT_IS_CLEAN" == true ]] ; then
        log_label "Unstaged Changes" "None (Clean)"
    else
        log_label "Unstaged Changes" "$GIT_NUM_UNSTAGED_LINES lines (dirty)"
    fi
}
