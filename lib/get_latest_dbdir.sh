#!/bin/sh
# Bryant Hansen


function get_latest_dbdir() {
    FUNCTION=${FUNCNAME[0]}

    # Imports; LIB_DIR must be in process space or the script should puke here
    . "$LIB_DIR"/log.sh

    unset dbdir
    dbdirs="$(ls -1d .filedb_* || echo "")"
    if [[ ! "$dbdirs" ]] ; then
        log "WARNING: a dbdir was not found.  Functionality might not be complete to handle this."
        echo ""
    else
        num_dbdirs="$(printf "${dbdirs}\n" | wc -l | cut -f 1)"
        latest_dbdir="$(printf "$dbdirs" | tail -n 1)"
        log "INFO: $num_dbdirs found.  The latest is: $latest_dbdir"
        log "INFO: Note: the 'latest' is determined by a directory-name-sort, which should have a sortable datestamp string"
        dbdir="$latest_dbdir"
        if [[ ! -d "$dbdir" ]] ; then
            log "ERROR: auto-caculated dbdir $dbdir does not exist.  (PWD=${PWD}).  abort"
            return 5
        fi
        db="$dbdir"/filedb.sqlite
        if [[ ! -f "$db" ]] ; then
            log "ERROR: the dbdir $dbdir does not not contain a db (filedb.sqlite).  abort"
            return 6
        fi
        echo "$dbdir"
    fi
}

