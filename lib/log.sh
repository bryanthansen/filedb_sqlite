#!/bin/sh
# Bryant Hansen

# Description: library containing log functions

## Functions

function log_header() {
    printf "$(date +%H:%M:%S.%N):" >&2
    [[ ! "${ME}" ]] || printf "${ME}:" >&2
    [[ ! "$FUNCTION" ]] || printf "%16s:" "$FUNCTION" >&2
    [[ ! "${*}" ]] || printf -- "${*}" >&2
}

function log() {
    log_header
    printf -- "$*\n" >&2
}

function log_label() {
    log_header
    if [[ "${#2}" -gt 30 ]] ; then
        printf "%s: %s\n" "$1" "$2" >&2
    else
        printf "%30s: " "$1" >&2
        shift
        [[ ! "$1" ]] || printf "$*\n" >&2
    fi
}

function log_section_start() {
    printf "\n" >&2
    log_header
    printf "###----------  %30s  ----------###\n"   "'$*'" >&2
}

function log_section_end() {
    log_header
    printf "###----------  %30s  ----------###\n" "'$*' END  " >&2
}

function log_append_line() {
    printf -- "$*" >&2
}

function log_quote() {
    block="$*"
    block2="${block//%/%%}"
    printf "'\n${block2}\n'\n" >&2
}
