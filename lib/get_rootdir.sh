#!/bin/sh
# Bryant Hansen

function get_rootdir() {
    dbdir="$1"
    if [[ "$dbdir" ]] ; then
        readlink -f "${dbdir}/.."
    else
        readlink -f .
    fi
}
