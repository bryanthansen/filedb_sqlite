#!/bin/bash
# Bryant Hansen

#time xzcat filedb.sqlite.files.sql.xz \
#| head -n 5000 \
#| grep -A 1 "^[ \t]*VALUES" \
#| grep -v "^--$\|^[ \t]*);\|[ \t]*VALUES" \
#| sed "s/^[ \t]*//;s/\,\ .*//" \
#| uniq -c

#for n in {1..100} ; do
#    echo -e "$(date):\n$(wc -l .filedb_DiskStation_20190504_181441/filedb.sqlite.files.dirs_scanned.[0-9]*)"
#    sleep 100
#done

# TODO: determine what to do with these
#dbdir=.filedb_DiskStation_20190504_181441
#pid=18790
#fd=3

read_file="$dbdir"/filedb.sqlite.files.sql.gz

monitor_proc_fd_read_pos() {
    file="$1"
    pid=$2
    fd=$3
    proc_fd_info=/proc/${pid}/fdinfo/${fd}
    echo "$(
              # Read the file pointer to check the position
              # Since the file is read in order and we know the size, we know
              # the percent completion and the estimated time remaining as well
              cat $proc_fd_info | grep "^pos"
           ) of $(
              # read the size (in bytes) of a file
              stat -c %s "$file"
           )"
}


printf "\n"
loop_delay=100
for n in {1..10} ; do
    date
    wc -l $(ls -1 "$dbdir"/filedb.sqlite.files.dirs_scanned.*[0-9] | tail -n 1)
    ls -alhtr "$dbdir" | tail -n 3
    if [[ $pid ]] ; then
        monitor_proc_fd_read_pos "$read_file" $pid 3
    fi
    sleep $loop_delay
done
