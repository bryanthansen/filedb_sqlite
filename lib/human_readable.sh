#!/bin/bash
# Bryant Hansen

# Description: print long numbers in a human readable format
# Support (MB, GB, TB or MiB, GiB, TiB)

# Why are there these trailing spaces?

function human_readable() {
    let TB_thresh=100*1000000000000
    let GB_thresh=100*1000000000
    let MB_thresh=100*1000000
    let KB_thresh=100*1000
    local n="$1"
    if [[ ! "$n" ]] ; then
        echo "null argument"
    elif [[ "${n//[0-9]/}" ]] ; then
        echo "non-numerical argument '${n}'"
    elif [[ "$n" -eq 0 ]] ; then
        printf "0             "
    elif [[ "$n" -lt $KB_thresh ]] ; then
        printf "%5d" "$n"
    elif [[ "$n" -gt $TB_thresh ]] ; then
        let out=n/1000000000000
        printf "%5d%s" "$out" "T"
    elif [[ "$n" -gt $GB_thresh ]] ; then
        let out=n/1000000000
        printf "%5d%s" "$out" "G"
    elif [[ "$n" -gt $MB_thresh ]] ; then
        let out=n/1000000
        printf "%5d%s" "$out" "M"
    elif [[ "$n" -gt $KB_thresh ]] ; then
        let out=n/1000
        printf "%5d%s" "$out" "k"
    else
        echo "invalid argument 2 '${n}'" >&2
        # just send back what we got in; except it may be slightly-parsed
        echo "$*"
    fi
}
