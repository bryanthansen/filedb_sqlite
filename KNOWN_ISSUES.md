* The setup for the file-hashing phase can fail with large datasets on low-memory devices; the initial query must be sharded.  Efforts have been made 
to use subqueries to improve the reliability with large datasets.

* If no files are in the DB, hash log files are still produced with a single record containing null entries.

    -> cat filedb.sqlite.hashes.sql 
    BEGIN TRANSACTION;
    INSERT INTO inode_md5_lookup (inode, md5, timestamp) VALUES (bash, "", DATETIME("now"));
    COMMIT;

    NOTE: inode_md5_lookup is an obsolete table name
