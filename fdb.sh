#!/bin/bash
# Bryant Hansen

# @brief wrapper for creating a file db and creating a report, with logging

set -e

## Constants and Defaults

ME="$(basename "$0")"

# MYDIR is set via the command-line call and takes the executable search path
# into account

# The goal is to find the path to the script so we can locate dependencies
# This method of finding the base directory of the script is designed to
# find it if:
#   - an absolute path to the script is used on the command line
#   - a relative path is used
#   - the script is in the executable path
# It returns the absolute path where the script resides
MYDIR="$(dirname "$(readlink -f "$(which "$0")")")"

SUBCOMMANDS="
    create
    status
    report
    fdb_report
    dedup
    execute
    monitor
    resume
    ping"
OPTIONS="$(for cmd in $SUBCOMMANDS ; do echo -n "$cmd | " ; done)"
#OPTIONS="[ ${OPTIONS%| }]"
OPTIONS="${OPTIONS%| }"
USAGE="$ME  $OPTIONS"

unset FUNCTION
unset NO_VERIFY

# NOTE: This line has no effect in the non-stand-alone version of the script
MIN_DEDUP_SIZE=8192


## Import Configs

# General config files (fdb.conf) in a search path
# All are imported; generally last-one wins, so order is defined from
# more-default to more-specific
# General order:
#    - global default constants
#    - calculated defaults
#    - application dir conf file
#    - global settings (etc) conf file
#    - user conf file
#    - current directory conf file
#    - command-line option
CONF_DIR_ETC=/etc/fdb
CONF_DIR_HOME=${HOME}/.config/fdb

printf "${ME}: $(date +%Y-%m-%d_%H:%M:%S) root=$PWD  args: '$*'\n" >&2

[[ "$CONF_DIR" ]] || CONF_DIR=.
for d in "$MYDIR" "$CONF_DIR_ETC" "$CONF_DIR_HOME" "$CONF_DIR" ; do
    [[ "$d" ]] || continue
    printf "${ME}: checking for config file at ${d}/fdb.conf\n" >&2
    [[ -d "$d" ]] || continue
    if [[ -f "$d"/fdb.conf ]] ; then
        . "$d"/fdb.conf
        printf "$ME import conf ${d}/fdb.conf\n" >&2
    fi
done

# Watch the NUM_PROCS variable
if [[ "$NUM_PROCS" ]] ; then
    printf "$ME INFO: NUM_PROCS read from config: $NUM_PROCS \n" >&2
fi

# LIB_DIR and SQL_DIR can come from:
#   1. environmental variables
#   2. hard-coded defaults
#   3. conf files, with a defined search path
# This is an ordered list; a var defined at any step will override the value
# of the previous step; last one wins
#
# The same is true for the conf file search path
# Configs are searched in a specific order, all are imported, and vars defined
# at the end of the search list will override those defined previously
#
# Commented tags are provided for a parser to strip the section when
# building the monolithic version of the script

# Tag: Start of dependency setup
[[ "$SQL_DIR"  ]] || SQL_DIR=./sql
[[ -d "$SQL_DIR" ]] || SQL_DIR="${MYDIR}/sql"
if [[ ! -d "$SQL_DIR" ]] ; then
    printf "$ME ERROR: SQL_DIR $SQL_DIR not found\n" >&2
    exit 2
fi

[[ "$LIB_DIR"  ]] || LIB_DIR=./lib
[[ -d "$LIB_DIR" ]] || LIB_DIR="${MYDIR}"/lib
if [[ ! -d "$LIB_DIR" ]] ; then
    printf "$ME ERROR: LIB_DIR $LIB_DIR not found\n" >&2
    exit 3
fi
# Tag: End of dependency setup

printf "${ME}: Import Dependencies: SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2
. "$LIB_DIR"/log.sh
. "$LIB_DIR"/create_fdb.sh
. "$LIB_DIR"/fdb_report.sh
. "$LIB_DIR"/dedup_report.sh
. "$LIB_DIR"/dedup_with_verify.sh
. "$LIB_DIR"/resume.sh
. "$LIB_DIR"/monitor.sh
. "$LIB_DIR"/show_version.sh


## Functions

usage() {
    printf "USAGE:\n  ${USAGE}\n" >&2
}


## ARGS

command="$1"


## Main

show_version

if [[ ! "$command" ]] ; then
    # start with commands (or detectable errors) that we do not log
    usage
    exit 1
fi

[[ "$LOG_DIR" ]]    || LOG_DIR=.filedb
[[ -d "$LOG_DIR" ]] || mkdir -p "$LOG_DIR"
LOGFILE="$LOG_DIR"/$(date +%Y%m%d_%H%M%S)_${command}.log

subfunction="$command"
[[ "$command" != create  ]] || subfunction=create_fdb
[[ "$command" != dedup   ]] || subfunction=dedup_report
[[ "$command" != execute ]] || subfunction=dedup_with_verify
[[ "$command" != status  ]] || subfunction=fdb_report

ret=0
if [[ "$subfunction" == monitor ]] ; then
    shift
    $subfunction $*
    ret=$?
    log "monitor '$*' terminated"
elif [[ "$subfunction" == ping ]] ; then
    echo "pong"
    ret=$?
    log "rx: ping, tx: pong"
else
    unset found
    for cmd in $SUBCOMMANDS ; do
        if [[ "$command" == "$cmd" ]] ; then
            (
                shift
                $subfunction $*
                ret=$?
                log "status $cmd '$*' complete, return code $ret"
            ) 2>&1 | tee "$LOGFILE"
            found=1
            break
        fi
    done
    if [[ ! "$found" ]] ; then
        log "ERROR: command $command (function ${subfunction}) not found"
        exit 2
    fi
fi
