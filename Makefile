# Bryant Hansen

.phony: help test clean standalone version FORCE

help:
	@echo "'make test' - run all tests"
	@echo "'make standalone' - produce the standalone version of the script"

FORCE:

standalone: FORCE version
	make -C standalone clean
	make -C standalone

version: FORCE
	make -C lib version.sh

test: clean lib/version.sh
	make -C test $@
	make -C standalone $@
	make -C lib $@
	@echo -e "\n$(shell pwd): ALL TESTS PASSED\n"

clean: FORCE
	make -C test $@
	make -C standalone $@
