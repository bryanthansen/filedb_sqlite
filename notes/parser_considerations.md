# Parser considerations - How to handle single- and double-quotes in filenames

## strategy #1: escape every double-quote that is not the first or the last double-quote

failed -- forgot exact reason TODO: update me

## strategy #2: escape all double-quotes, but then un-escape the 2 that should not be escaped

failed due to lack of ability to escape double-quotes in SQL update queries in bash

## strategy #3:

  * add a carriage return right before the name
  * path/file name exist on their own line
  * delimited with single quotes as first and last character
  * identified by delimiter in position 1
  * which is marked by a single delimiter (a single quote) in position 1
    sed "/^'/s/'/''/g;s/''$/'/;s/''/'/"
  * This seems to work well and pass tests
  * It's a bit messy and inefficient

## strategy #4:

  # use a specific tag for the filename
  # the end of the tag to the end of the line delimit the path/file name
  # replace all single quotes with 2 single-quotes
  # replace the 2-single-quote sequence at the beginning and the end with 1-single-quote

Current working solution

## strategy #5: rely on a different parsing syntax for db storage.  XML?
