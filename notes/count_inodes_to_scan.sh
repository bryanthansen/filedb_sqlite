# This was an early attempt
# It appears that subqueries may be faster with large datasets on slow devices

"PRAGMA query_only=true;
SELECT COUNT(files.inode)
FROM files
LEFT JOIN hashes
ON files.inode = hashes.inode
WHERE hashes.inode IS NULL
ORDER BY files.inode;
" | sqlite3 filedb.sqlite

