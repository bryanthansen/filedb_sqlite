#!/bin/bash
# Bryant Hansen

ME="$(basename "$0")"
LOG="${ME%.sh}.log"
ERR="${ME%.sh}.err"

MIN_SIZE=8192
NUM_DUPS=10

# DT="$(date +%Y%m%d_%H%M%S)"
# LOG="${ME%.sh}_${DT}.log"
# ERR="${ME%.sh}_${DT}.err"

sql="
    SELECT
        md5,
        '''' || directories.name || '/' || dup_sizes.name || '''',
        hashes.inode,
        dup_sizes.size,
        dup_sizes.num_dups,
        dup_sizes.dup_size
    FROM (
        SELECT
            dirid,
            name,
            inode,
            size,
            (COUNT(inode) - 1) AS num_dups,
            ((COUNT(inode) - 1) * size) AS dup_size
        FROM files
        WHERE inode IN (
            SELECT large_dups.inode AS inode
            FROM (
                SELECT inode, md5
                FROM hashes
                WHERE inode IN (
                    SELECT inode
                    FROM files
                    WHERE size > $MIN_SIZE
                )
                GROUP BY md5
                HAVING COUNT(inode) > 1
            ) AS large_dups
        )
        GROUP BY inode
        ORDER BY dup_size DESC
        LIMIT $NUM_DUPS
    ) AS dup_sizes
    LEFT JOIN hashes on dup_sizes.inode = hashes.inode
    LEFT JOIN directories on directories.id = dup_sizes.dirid
"

echo -e "sql: $sql" > "$ERR"
(
    time sqlite3 filedb.sqlite "$sql" > "$LOG"
) 2>> "$ERR" &
