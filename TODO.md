# filedb_sqlite TODO

* TODO: shards/chunks of scanned directories should operate in parallel, with a specified number of processes.  This is likely an easier and 
more-efficient method of parallelization than the current method of a hash-function-subshell in an xargs argument with the parallel-execution feature 
enabled (-Pn -- P1, P2, P3...Pn)

* TODO: Add execution stage monitor (and all running stages)

* TODO: Add user and group id/name lookup tables

* TODO: Options to replace (or not replace) files which have non-matching inode data
    - ctime may always be different
    - ownership changes may or may not trigger a destructive hard-link replacement

* TODO: Add a warning level of identical file context with different ownership and/or
  permissions

* TODO: Add optional handling of differences - at the moment, changes

* TODO: Add a warning level for mtime/ctime changes

* TODO: Complete live-monitoring functionality

* TODO: Create test to verify that, if we have no files in the DB, then no hashes should be logged and no hash records should be in the DB

* TODO: Testing of resume feature

* TODO: Remove non-sharded operations and logfile names

* TODO: Add an adaption layer for mysql

* TODO: move all SQL intermediate output to an sql (or .sql) folder

* TODO: instead of using the SQL DATETIME("now") function to record time, the script should probably try to insert a time that the file was read; check the proper format
