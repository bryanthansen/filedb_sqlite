# SQLite File Database and Deduplicator

_(c)2019 Bryant Hansen -- License: LGPLv3_

This is a set of shell scripts which analyze a filesystem in exquisite detail

- TOC
{:toc}

## Use Cases

* Manage backup archives which contain a signficant amount of duplicate
  information (media, OS installations, file-tree versions, directories
  which have been renamed, triggering a new backup copy, ...)

* Monitor changes to filesystems with the ability to query the finest details
  Examples:
    - What are the largest 10 files in directory tree ./some/dir/?
    - The total number of files greater than 1 block in size on a device?
    - How many copies of a single file are owned by the same owner?
    - Have any files under a directory tree changed unexpectedly in the last 5
      years?
    - How many identical files are resulting in duplicate data on a drive?

* Compare the unique and duplicate files across multiple devices, partitions,
  filesystems, or directory trees

* Facilitate the removal of obsolete data with traceability of what has been
  removed


## Functionality

* Create a filesystem database
    - device and partition information
    - all files and directories
    - inode properties
    - file hashes

* Create an executable report to replace duplicate files with hard links

* Facilitate the safe and efficient replacement of duplicate information

* Record deduplication operations (before/after states) and maintain the
  theoretical possiblity to reverse operations (compress, but don't destroy)

* Generate analytical reports of filesystem state
    - largest files
    - greatest duplicate space used
    - support index-based filesystem querying

* Maintain a record of backups with device and date information.  Monitor and
  report redundancy across devices.

* Maintain a historical record of file changes

* Provides features similar to snapshotting on filesystems which do not
  provide native support for it

* 


The scripts utilize *sqlite* as the database interface; this is a dependency

Some attempts have been made to support the oldest, most-minimal shells.
Bash does not seem universal on all embedded devies, so sh should be
supported.
Ubuntu handles the #!/bin/sh shebang differently - known issues.  Does not
support [[ and ]] conditional operators

## Dependencies

All dependencies should be standard in the respective distribution

* ![SQlite3)(https://sqlite.org/)
* sh Shell or ![bash](https://wikipedia.org/wiki/Bash_%28Unix_shell%29) shell
* Optional: GNU Make for build and test automation

### Compression and Decompression

* xz seems to be universal-enough to select for compression
* gzip is faster for both the encode and decode; better for test data
* xzcat is used in some places
* gzip or bzip (with zcat and bzcat respectively) could be easily-substituted

### Hashing

* md5 is the selected algorithm; md5sum is the binary
* sha256 could be easily-substituted, with an obvious performance penalty
* for a super-minimal and fast solution, could size + crc32 be used?
* hashing is performed in parallel via creative use of the xargs command

## Language Alternatives

These are the various language alternatives that your author has considered.
They are all as universal, widely-available, and installed-by-default as
possible.  It runs on anything without having to install anything.  Efficiency
is a bonus.
_Disclaimer: this list is somewhat-biased to personal preferences_

  * Bash
      - fairly-universal, but not 100%
      - efficient enough
        - in high-volume datasets, most of the work is done in compiled binaries
        - minimal processing and loops are performed in the script

  * sh/ash
      - A subset of Bash which the bash shell can also execute
      - Most-universal language possible
      - Some optional features should be removed like, (( )), =~, 
          - Vaguely-supported optional features: [[ ]], {a..b}

  * Python
      - Far-better structure
      - Adds a possibly-difficult dependency for very-minimal Linux systems

  * C
      - Smaller, more-efficient, more-professional
      - Requires custom compilation for each target
          + Not easy for a wide variety of NAS's


## Noteable Features

* Deduplicates via hard links so filesystems so backup trees maintain the same
  properties

* Handles filenames containing a wide range of special characters

* Performs hashing in parallel

* Minimal dependencies - can run on various embedded devices, including a
  Synology NAS, a Raspberry Pi, or the default install of a vast-majority of
  Linux distros


## Individual Functions

### fdb.sh

A top-level script which provides an interface to all functionality in the suite

Typing this without arguments produces a brief help menu

### fdb.sh create

Produces an sqlite database, cataloging data of all files and
directories in the current tree.

It does *not* descend into mountpoints.

### fdb.sh dedup

Produces and executable report, which can be used to replace all
duplicates with hard links.

### fdb.sh execute

Safely executes the dedup report, performing the deduplicate
operation; it verifies that the MD5's have not changed since the report was
produced

### fdb.sh status

Provides summary information about the duplicate files found and about
filesystem scans which may have been interrupted at some stage of scanning

### fdb.sh resume

Resume a previous run that was not completed

### fdb.sh monitor

Provide progress updates on an operation-in-progress


## Technical Notes

File and directory names containing the LineFeed character (0x0A) have been
shown to cause issues in the deduplication process.  Therefore, these
file and directory names are excluded from the deduplication report.  They
are, however, added to the database.


## Challenges

The scan operation is very long.  This is intended to function on multi-TB
storage arrays.  Hashing all files on a nearly-full, 3TB partition on a USB
drive, connected to a Raspberry Pi Zero, may take a little while.


## Target Platforms

* GNU/Linux on 32- or 64-bit x86/AMD or Atom - standard test/production platform
    - Gentoo
    - Ubuntu
    - Knoppix
* Synology - standard test/production platform
* Raspberry Pi - standard test/production platform
* Cygwin - untested idea
* Solaris/SmartOS - untested idea


## Future Development

* Deletion schedules - notify the user of trees that may be recommended for
  removal
    - Even consider classes of files with a specified lifetime
    - Identify biggest users when deletion decisions need to be considered
    - Warning: auto-deletion in general could be a tad bit risky

* Incorporate the get-compressibility-ratio function from another source tree
    - Analyze the compressibility of a specified file
    - Compare both compression ratios and processing times
    - Compare both algorithms and settings
    - Compare single-threaded implementations against parallel
    - Facilitate the compression of all hard-linked instances of a file
      within a directory tree


## WARNING: use at your own risk - may have unintended side-effects

* Removing duplicate inodes removes the information associated with them; user,
  group, create/modify/access times, ACL's, etc. of the original inode are lost.
  They remain in the DB and can theoretically be restored.  However, restore
  functionality is not yet planned.


## TODO.md

* Determine best way to include sections into the main document

## KNOWN_ISSUES.md

## Example

TODO:

* Create a deduplicate example of multiple versions of a base install of a
  GNU/Linux OS
* 
