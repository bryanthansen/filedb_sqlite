# Bryant Hansen

sql_query_inodes_to_hash="
                      PRAGMA query_only=true;
                      SELECT files.inode, directories.name || '/' || files.name
                      FROM files
                      LEFT JOIN directories
                      ON directories.id = files.dirid
                      LEFT JOIN hashes
                      ON files.inode = hashes.inode
                      WHERE files.name NOT LIKE '%' || CHAR(10) || '%'
                      AND hashes.inode IS NULL
                      GROUP BY files.inode
                      ORDER BY files.inode;
                  "

function query_inode_chunk_to_hash() {
    local start_inode="$1"
    local end_inode="$2"
    echo "
            PRAGMA query_only=true;
            SELECT files.inode, directories.name || '/' || files.name
            FROM files
            LEFT JOIN directories
            ON directories.id = files.dirid
            LEFT JOIN hashes
            ON files.inode = hashes.inode
            WHERE files.name NOT LIKE '%' || CHAR(10) || '%'
            AND hashes.inode IS NULL
            AND files.inode >= $start_inode
            AND files.inode <= $end_inode
            GROUP BY files.inode
            ORDER BY files.inode;
         "
}
