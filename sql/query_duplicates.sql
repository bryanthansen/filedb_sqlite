# Bryant Hansen

[[ "$MIN_DEDUP_SIZE" -lt 0 ]] || MIN_DEDUP_SIZE=8192

sql_query_duplicates="
    PRAGMA query_only=true;
    SELECT hashes.md5, files.inode,
           '''' || directories.name || '/' || files.name || ''''
    FROM files
    LEFT JOIN directories
    ON files.dirid = directories.id
    LEFT JOIN hashes
    ON files.inode = hashes.inode
    WHERE hashes.md5 IN (
        SELECT md5
        FROM hashes
        GROUP BY hashes.md5
        HAVING COUNT(inode) > 1
    )
    AND files.size >= $MIN_DEDUP_SIZE
    AND files.name NOT LIKE '%' || char(10) || '%'
    ORDER BY hashes.md5,
             files.ctime DESC,
             files.mtime ASC,
             files.inode;
"

[[ "$MD5_PREFIX" ]] || MD5_PREFIX=00000
sql_query_duplicate_chunk="
    PRAGMA query_only=true;
    SELECT hashes.md5, files.inode,
           '''' || directories.name || '/' || files.name || ''''
    FROM files
    LEFT JOIN hashes ON files.inode = hashes.inode
    LEFT JOIN directories ON files.dirid = directories.id
    WHERE files.size >= $MIN_DEDUP_SIZE
    AND files.inode IN (
        SELECT inode
        FROM hashes
        WHERE md5 IN (
            SELECT md5
            FROM hashes
            WHERE md5 like '${MD5_PREFIX}%'
            GROUP BY md5
            HAVING COUNT(hashes.inode) > 1)
        )
    );
"

sql_query_duplicate_chunk2="
    PRAGMA query_only=true;
    SELECT hashes.md5, files.inode,
           '''' || directories.name || '/' || files.name || ''''
    WHERE files.inode IN (
        SELECT inode
        FROM hashes
        WHERE md5 IN (
            SELECT md5
            FROM hashes
            WHERE md5 LIKE '${MD5_PREFIX}%'
            AND inode IN (
                SELECT inode
                FROM files
                WHERE size >= $MIN_DEDUP_SIZE
            )
            GROUP BY md5
            HAVING COUNT(hashes.inode) > 1
        )
    )
    LEFT JOIN hashes      ON files.inode = hashes.inode
    LEFT JOIN directories ON files.dirid = directories.id;
"

function gen_query_duplicates() {
    local md5_prefix="$1"
    echo "
            PRAGMA query_only=true;
            SELECT large_dups.md5, files.inode,
                  '''' || directories.name || '/' || files.name || ''''
            FROM (
                SELECT md5, inode
                FROM hashes
                WHERE md5 IN (
                    SELECT md5
                    FROM hashes
                    WHERE md5 LIKE '${md5_prefix}%'
                    AND inode IN (
                        SELECT inode
                        FROM files
                        WHERE size >= $MIN_DEDUP_SIZE
                    )
                    GROUP BY md5
                    HAVING COUNT(inode) > 1
                )
                ORDER BY md5, inode
            ) AS large_dups
            JOIN files       ON large_dups.inode = files.inode
            JOIN directories ON files.dirid      = directories.id
         "
}
