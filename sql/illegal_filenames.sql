# Bryant Hansen

# SQL statements to query the db for illegal filenames
# Current invalid characters are only linefeeds (10 or 0xA)

sql_illegal_filenames="
    PRAGMA query_only=true;
    SELECT '''' || directories.name || '/' || files.name || '''' AS pathname
    FROM files
    LEFT JOIN directories ON files.dirid = directories.id
    WHERE files.name LIKE '%' || char(10) || '%';
"

sql_num_illegal_filenames="
    PRAGMA query_only=true;
    SELECT COUNT(id)
    FROM files
    WHERE files.name LIKE '%' || char(10) || '%';
"
