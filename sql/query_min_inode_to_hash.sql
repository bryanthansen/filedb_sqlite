# Bryant Hansen

sql_query_min_inode_to_hash="
                              PRAGMA query_only=true;
                              SELECT MIN(files.inode)
                              FROM files
                              LEFT JOIN hashes
                              ON files.inode = hashes.inode
                              WHERE hashes.md5 IS NULL;
"
