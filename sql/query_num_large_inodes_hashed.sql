# Bryant Hansen

[[ "$MIN_DEDUP_SIZE" -lt 0 ]] || MIN_DEDUP_SIZE=8192

sql_num_large_inodes_hashed="
    PRAGMA query_only=true;
    SELECT COUNT(DISTINCT inode)
    FROM hashes
    WHERE inode IN (SELECT inode FROM files WHERE size > $MIN_DEDUP_SIZE)
"
