# create_db.sql

# Description: the SQL statement to create the database

create_db_sql="
    PRAGMA foreign_keys=OFF;
    BEGIN TRANSACTION;
    CREATE TABLE directories (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name          TEXT    NOT NULL,
          size          INT     NOT NULL,
          ctime         TEXT,
          mtime         TEXT    NOT NULL,
          uid           INT,
          gid           INT,
          perm          INT,
          inode         INT     NOT NULL,
          nlinks        INT,
          device        INT,
          timestamp     TEXT    NOT NULL
      );
    CREATE TABLE files (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          dirid         INT     NOT NULL,
          name          TEXT    NOT NULL,
          size          INT     NOT NULL,
          ctime         TEXT,
          mtime         TEXT    NOT NULL,
          uid           INT,
          gid           INT,
          perm          INT,
          inode         INT     NOT NULL,
          nlinks        INT,
          timestamp     TEXT    NOT NULL
      );
    CREATE TABLE hashes (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          inode         INT     NOT NULL,
          md5           VARCHAR(32),
          timestamp     TEXT    NOT NULL
      );
    CREATE TABLE settings (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          key           VARCHAR(255),
          value         TEXT,
          timestamp     TEXT    NOT NULL
      );
    CREATE TABLE statistics (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          key           VARCHAR(255),
          value         TEXT,
          timestamp     TEXT    NOT NULL
      );
    CREATE TABLE users (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name          TEXT    NOT NULL,
          uid           INT     NOT NULL,
          gid           INT,
          description   TEXT,
          home          TEXT,
          shell         TEXT,
          mtime         TEXT    NOT NULL
      );
    CREATE TABLE groups (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          name          TEXT    NOT NULL,
          gid           INT,
          user_list     TEXT,
          mtime         TEXT    NOT NULL
      );
    CREATE TABLE device (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          path          TEXT    NOT NULL,
          model         TEXT,
          serial_number TEXT,
          firmware      TEXT,
          transport     TEXT,
          form_factor   TEXT,
          rotation      TEXT,
          dma           TEXT,
          pio           TEXT,
          capacity      INT,
          mtime         TEXT    NOT NULL
      );
    CREATE TABLE partition (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          path          TEXT    NOT NULL,
          gid           INT,
          fs_type       TEXT,
          flags         TEXT,
          block_size    INT,
          capacity      INT,
          mtime         TEXT    NOT NULL
      );
    CREATE TABLE device_probe (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          device_file   TEXT,
          probe_command TEXT,
          stdout        TEXT,
          stderr        TEXT,
          retval        INT,
          timestamp     TEXT    NOT NULL
      );

    CREATE UNIQUE INDEX unique_dirs        ON directories  (name);
    CREATE UNIQUE INDEX unique_files       ON files        (dirid, name);
    CREATE        INDEX files_inodes       ON files        (inode);
    CREATE        INDEX hashes_inodes      ON hashes       (inode);
    CREATE        INDEX hashes_md5         ON hashes       (md5);
    CREATE UNIQUE INDEX unique_inode_hash  ON hashes       (inode, md5);

    DELETE FROM sqlite_sequence;
    COMMIT;
"
