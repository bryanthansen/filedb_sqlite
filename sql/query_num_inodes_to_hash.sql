# Bryant Hansen

sql_num_inodes_to_hash="
                              PRAGMA query_only=true;
                              SELECT COUNT(DISTINCT files.inode)
                              FROM files
                              LEFT JOIN hashes
                              ON files.inode = hashes.inode
                              WHERE hashes.md5 IS NULL;
"

_sql_num_inodes_to_hash__experiment="
                              PRAGMA query_only=true;
                              SELECT COUNT(DISTINCT files.inode)
                              FROM files
                              WHERE inode NOT IN (
                                  SELECT inode FROM hashes
                              );
"
