# Bryant Hansen

[[ "$MIN_DEDUP_SIZE" -lt 0 ]] || MIN_DEDUP_SIZE=8192

sql_num_unique_hashes="
    PRAGMA query_only=true;
    SELECT COUNT(DISTINCT md5)
    FROM hashes
    WHERE inode IN (SELECT inode FROM files WHERE size > $MIN_DEDUP_SIZE)
"
