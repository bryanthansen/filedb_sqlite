#!/bin/sh
# Bryant Hansen

# Perform an SQL query which shows the files which are the top users of
# duplicated space within the directory tree

#   - Only inspect files larger than MIN_DEDUP_SIZE (typically 2 4096-byte
#     blocks)
#   - Count the number of duplicate inodes (ie. 3 identical files taking 3
#     different inodes result in 2 duplicate files)
#   - Also provide an overall sum as an query add-on

[[ "$MIN_DEDUP_SIZE" -lt 0 ]] || MIN_DEDUP_SIZE=8192
[[ "$NUM_DUPS" -lt 0 ]] || NUM_DUPS=10

sql_file_report="
    PRAGMA query_only=true;
    SELECT
        md5,
        hashes.inode,
        dup_sizes.size,
        dup_sizes.num_dups,
        dup_sizes.dup_size,
        '''' || directories.name || '/' || dup_sizes.name || '''' AS pathname
    FROM (
        SELECT
            dirid,
            name,
            inode,
            size,
            (COUNT(inode) - 1) AS num_dups,
            ((COUNT(inode) - 1) * size) AS dup_size
        FROM files
        WHERE inode IN (
            SELECT large_dups.inode AS inode
            FROM (
                SELECT inode
                FROM hashes
                WHERE inode IN (
                    SELECT inode
                    FROM files
                    WHERE size > $MIN_DEDUP_SIZE
                )
                GROUP BY md5
                HAVING COUNT(inode) > 1
            ) AS large_dups
        )
        GROUP BY inode
        ORDER BY dup_size DESC
        LIMIT $NUM_DUPS
    ) AS dup_sizes
    LEFT JOIN hashes      ON dup_sizes.inode = hashes.inode
    LEFT JOIN directories ON dup_sizes.dirid = directories.id;
"

# Method #1 of counting the number of duplicate files
sql_num_dup_files1="
    PRAGMA query_only=true;
    SELECT SUM(num_dups.num_dups) AS total_num_dups
    FROM (
        SELECT
            (COUNT(inode) - 1) AS num_inode_dups
        FROM (
            SELECT large_dups.inode AS inode
            FROM (
                SELECT inode
                FROM hashes
                WHERE inode IN (
                    SELECT inode
                    FROM files
                    WHERE size > $MIN_DEDUP_SIZE
                )
                GROUP BY md5
                HAVING COUNT(inode) > 1
            ) AS large_dups
        )
        GROUP BY inode
    ) AS num_dups;
"

# Method #2 of counting the number of duplicate files
sql_num_dup_files2="
    PRAGMA query_only=true;
    SELECT COUNT(DISTINCT inode) - COUNT(DISTINCT md5) AS duplicates
    FROM hashes
    WHERE inode IN (SELECT inode FROM files WHERE size > $MIN_DEDUP_SIZE)
"

sql_num_dup_files="$sql_num_dup_files2"
