# Bryant Hansen

[[ "$MIN_DEDUP_SIZE" -lt 0 ]] || MIN_DEDUP_SIZE=8192

sql_total_dup_size="
    PRAGMA query_only=true;
    SELECT SUM(dup_sizes.dup_size)
    FROM (
        SELECT ((COUNT(inode) - 1) * size) AS dup_size
        FROM files
        WHERE inode IN (
            SELECT large_dups.inode AS inode
            FROM (
                SELECT inode, md5
                FROM hashes
                WHERE inode IN (
                    SELECT inode
                    FROM files
                    WHERE size > $MIN_DEDUP_SIZE
                )
                GROUP BY md5
                HAVING COUNT(inode) > 1
            ) AS large_dups
        )
        GROUP BY inode
    ) AS dup_sizes;
"
