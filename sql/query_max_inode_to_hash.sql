# Bryant Hansen

sql_query_max_inode_to_hash="
                              PRAGMA query_only=true;
                              SELECT MAX(files.inode)
                              FROM files
                              LEFT JOIN hashes
                              ON files.inode = hashes.inode
                              WHERE hashes.md5 IS NULL;
"
