# Requirements

* Modularize script - use a makefile to build a stand-alone script (plus testing)

* Provide statistics:
    - progress during long operations
    - overall sizes
    - measured sizes
    - hdparm; consider an md5 log
    - performance rates - for both optimization and benchmarking
        + logging can involve lots of extra disk space; consider options for
            - capping file sizes
            - buffering (like a fifo)
            - specifying a ramdisk dir

* Functional get_dbdir - provide a file, a directory, or no arg and get an ingelligent, useful, safe response

* Minimizes database locking; determine performance trade-offs

* Structured to operate as both a modular script with many small imports and an all-in-one stand-alone script
    - A makefile takes the individual components and combines them into an auto-generated one-script-rules-them-all, with no imports
    - Advantages from both models can be realized
        + Eliminates complexity
        + Improves the structure

* Convenient test for a locked sqlite db

* Live monitoring of active, long-running operation

* Be able to resume long operations at various points in time

* TODO: decide if multiple inodes with different timestamps should be supported
