#!/bin/bash
# Bryant Hansen

# Description: dump files with source line replaced with the specified file

function find_file() {
    f="$1"
    f="${f#. }"
    f="${f//\"\$\{LIB_DIR\}\"/..\/lib}"
    f="${f//\"\$LIB_DIR\"/..\/lib}"
    f="${f//\"\$\{SQL_DIR\}\"/..\/sql}"
    f="${f//\"\$SQL_DIR\"/..\/sql}"
    echo "$f"
}

function replace_source_with_source() {
    # TODO: use SED variables
    SED_PRINT_SOURCE_LINES='/^[ \t]*\.[ \t]/p'

    # TODO: run this recursively with a runaway-loop check
    sed -n "$SED_PRINT_SOURCE_LINES" \
    | while read l ; do
        if [[ "${l:0:2}" == ". " ]] ; then
            src="$(find_file "$l")"
            printf "### '$l' is a source line; src=${src}\n" >&2
            if [[ -f "$src" ]] ; then
                echo "$src"
                cat "$src" \
                | sed -n "$SED_PRINT_SOURCE_LINES" \
                | while read l2 ; do
                    src2="$(find_file "$l2")"
                    if [[ -f "$src2" ]] ; then
                        echo "### embedded source l1: '${src}'" \
                            "contains '${src2}'" >&2
                        echo "$src2"
                        cat "$src2" \
                        | sed -n "$SED_PRINT_SOURCE_LINES" \
                        | while read l3 ; do
                            src3="$(find_file "$l3")"
                            if [[ -f "$src3" ]] ; then
                                echo "### embedded source l2: '${src2}'" \
                                    "contains '${src3}'" >&2
                                echo "$src3"
                                cat "$src3" \
                                | sed -n "$SED_PRINT_SOURCE_LINES" \
                                | while read l4 ; do
                                    src4="$(find_file "$l4")"
                                    if [[ -f "$src4" ]] ; then
                                        echo "### embedded source l3: '${src3}'" \
                                            "contains '${src4}'" >&2
                                        echo "$src4"
                                        cat "$src4" \
                                        | sed -n "$SED_PRINT_SOURCE_LINES" \
                                        | while read l5 ; do
                                            src5="$(find_file "$l5")"
                                            if [[ -f "$src5" ]] ; then
                                                echo "### embedded source l4: '${src4}'" \
                                                    "contains '${src5}'" >&2
                                                echo "$src5"
                                            else
                                                echo "### ERROR: src5 file: '${src5}'" \
                                                    "not found (PWD=${PWD})" >&2
                                            fi
                                        done
                                    else
                                        echo "### ERROR: src4 file: '${src4}'" \
                                            "not found (PWD=${PWD})" >&2
                                    fi
                                done
                            else
                                echo "### ERROR: src3 file: '${src3}'" \
                                    "not found (PWD=${PWD})" >&2
                            fi
                        done
                    else
                        echo "### ERROR: src2 file: '${src2}'" \
                            "not found (PWD=${PWD})" >&2
                    fi
                done
            else
                echo "### ERROR: src file '${src}' not found (PWD=${PWD})" >&2
            fi
        else
            echo "### $0 WARNING: $l is not a source line" >&2
        fi
    done \
    | sort \
    | uniq \
    | while read src ; do
        echo -e "### inserting source $src" >&2
        echo -e "\n### source $src"
        cat "$src" | sed  's/^[ \t]*\.[ \t]/\#\#\#\ &/'
        echo -e "### end of source ${src}\n"
    done \
    | sed '/\#\ Setup\ import\ dirs/,/\#\ Setup\ import\ dir\ end/d'
}


echo "### Processing sources with $0"

# TODO: consider limited recursive call to dump source files
# Perform 3 levels of detection for hard-coded import/source files
# TODO: failing now due to lack of recursive

# TODO: figure out how the hell to do this recursively


replace_source_with_source


echo "### $0 complete."
