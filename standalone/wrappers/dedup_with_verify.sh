#!/bin/sh
# Bryant Hansen

# This script more-safely-executes the hard link lines in a duplicate report
# A duplicate report is in the form:
#   ln -f  base_file  duplicate_file
#
# It is a problem if the duplicate file is changed between the generation of
# the report and the 

# Option: abort-on-fail or keep-going

set -e

## Constants and Defaults

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="${ME}  [  -l | --already-linked-ok  ]  [  DBDIR  ]"

DUPLICATE_REPORT_FILENAME=duplicate_report.sh
VERIFY=1
unset ALREADY_LINKED_IS_OK


## Import Configs

# General config files (fdb.conf) in a search path
# All are imported; generally last-one wins, so order is defined from
# more-default to more-specific
# General order:
#    - global default constants
#    - calculated defaults
#    - application dir conf file
#    - global settings (etc) conf file
#    - user conf file
#    - current directory conf file
#    - command-line option
CONF_DIR_ETC=/etc/fdb
CONF_DIR_HOME=${HOME}/.config/fdb

[[ "$CONF_DIR" ]] || CONF_DIR=.
printf "$ME  CONF_DIR: $CONF_DIR \n" >&2
for d in "$MYDIR" "$CONF_DIR_ETC" "$CONF_DIR_HOME" "$CONF_DIR" ; do
    [[ "$d" ]] || continue
    [[ -d "$d" ]] || continue
    printf "${ME}: checking for config dir ${d}/fdb.conf...\n" >&2
    if [[ -f "$d"/fdb.conf ]] ; then
        . "$d"/fdb.conf
        printf "import conf ${d}/fdb.conf\n" >&2
    fi
done

[[ "$SQL_DIR"  ]] || SQL_DIR=../sql
if [[ ! -d "$SQL_DIR" ]] ; then
    if [[ "$SQL_DIR" ]] ; then
        printf "WARNING: SQL_DIR $SQL_DIR does not exist as a directory\n" >&2
    fi
    SQL_DIR="${MYDIR}/../../sql"
fi
if [[ ! -d "$SQL_DIR" ]] ; then
    printf "ERROR: SQL_DIR $SQL_DIR not found\n" >&2
    exit 2
fi

[[ "$LIB_DIR"  ]] || LIB_DIR=./lib
if [[ ! -d "$LIB_DIR" ]] ; then
    if [[ "$LIB_DIR" ]] ; then
        printf "WARNING: LIB_DIR $LIB_DIR does not exist as a directory\n" >&2
    fi
    LIB_DIR="${MYDIR}"/../../lib
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    printf "ERROR: LIB_DIR $LIB_DIR not found\n" >&2
    exit 3
fi


## Import Dependencies

printf "${ME}: PWD=$PWD  CONF_DIR=$CONF_DIR  SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2
. "$LIB_DIR"/log.sh
. "$LIB_DIR"/${ME}


## Process Arguments

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]] || [[ "$1" == "help" ]] ; then
    echo "USAGE: " >&2
    echo "   $USAGE" >&2
    exit 1
fi

# dash-options
while [[ "${1:0:1}" == '-' ]] ; do
    case "$1" in 
    "--already-linked-ok"|"-l")
        ALREADY_LINKED_IS_OK=1
        shift
        ;;
    "--verbose"|"-v")
        VERBOSE=1
        shift
        ;;
    "--")
        shift
        break
        ;;
    *)
        echo "$ME ERROR: unknown option: $1  abort." >&2
        exit 4
        ;;
    esac
done

# Filedb dir
if [[ "$1" ]] ; then
    dbdir="$1"
fi
dbdir="${dbdir%%\/}"
if [[ ! -d "$1" ]] ; then
    echo "$ME ERROR: dbdir '${dbdir}' does not exist as a directory. abort" >&2
    exit 2
fi
duplicate_report="${dbdir}/${DUPLICATE_REPORT_FILENAME}"


## Main

log "start"
unset args
[[ "$VERBOSE"              ]] && args="$args -v"
[[ "$ALREADY_LINKED_IS_OK" ]] && args="$args --already-linked-ok"
[[ "$args" ]] && args="$args -- "
dedup_with_verify $args "$dbdir" "$duplicate_report"
log "done"
