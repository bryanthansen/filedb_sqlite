# Wrapper scripts for higher-level functions

This is to serve as a basis to build stand-alone scripts

The source lines (.) are intended to be replaced by the source in a Make process; the resulting scripts should be stand-alone

## Individual Scripts

This information is copied from the original README in the root of the repo.
Now descriptions will exist in 2 places.  Bad.
Consider a simple scheme to synchronize these descriptions, or at least a test to verify

### fdb.sh

A top-level script which calls create_fdb.sh and dedup_report.sh.  It creates
the database, produces the report, and performs setup, cleanup, and logging
operations

### create_fdb.sh

This script produces an sqlite database, cataloging data of all files and
directories in the current tree.  It does *not* descend into mountpoints.

### dedup_report.sh

This script produces and executable report, which can be used to replace all
duplicates with hard links.

### dedup_with_verify.sh

This script safely executes the dedup report, performing the deduplicate
operation; it verifies that the MD5's have not changed since the report was
produced

### duplicate_size_report.sh

This script provides summary information about the duplicate files found

