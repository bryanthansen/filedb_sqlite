#!/bin/bash
# Bryant Hansen

# Take a file database and dump a set of hard link commands that will
# deduplicate the file set

# Information will be lost

# When we replace a duplicate inode with another inode, the original inode
# properties will be lost.  This includes:
#    ownership
#    permissions
#    change/modify/access times
#    any other inode properties?
# The ORDER BY clause controls how to prioritize which inode properties will be used as the reference and kept
# The first file in the list is the reference inode; other inodes will be discarded

# This inode data will still exist in the database, allowing the theoretical possibility of reserving the operation.
# However, a restore utility does not yet exist.

# If filedb is not specified on the command line, then the current directory
# is assumed to be the db dir

set -e

## Constants and Defaults

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="$ME [  filedb_dir  ]"

FILEDB_NAME=filedb.sqlite
MIN_SIZE=8192


## Import Configs

# General config files (fdb.conf) in a search path
# All are imported; generally last-one wins, so order is defined from
# more-default to more-specific
# General order:
#    - global default constants
#    - calculated defaults
#    - application dir conf file
#    - global settings (etc) conf file
#    - user conf file
#    - current directory conf file
#    - command-line option
CONF_DIR_ETC=/etc/fdb
CONF_DIR_HOME=${HOME}/.config/fdb

[[ "$CONF_DIR" ]] || CONF_DIR=.
printf "$ME  CONF_DIR: $CONF_DIR \n" >&2
for d in "$MYDIR" "$CONF_DIR_ETC" "$CONF_DIR_HOME" "$CONF_DIR" ; do
    [[ "$d" ]] || continue
    [[ -d "$d" ]] || continue
    printf "${ME}: checking for config dir ${d}/fdb.conf...\n" >&2
    if [[ -f "$d"/fdb.conf ]] ; then
        . "$d"/fdb.conf
        printf "import conf ${d}/fdb.conf\n" >&2
    fi
done

[[ "$SQL_DIR"  ]] || SQL_DIR=../sql
if [[ ! -d "$SQL_DIR" ]] ; then
    if [[ "$SQL_DIR" ]] ; then
        printf "WARNING: SQL_DIR $SQL_DIR does not exist as a directory\n" >&2
    fi
    SQL_DIR="${MYDIR}/../../sql"
fi
if [[ ! -d "$SQL_DIR" ]] ; then
    printf "ERROR: SQL_DIR $SQL_DIR not found\n" >&2
    exit 2
fi

[[ "$LIB_DIR"  ]] || LIB_DIR=./lib
if [[ ! -d "$LIB_DIR" ]] ; then
    if [[ "$LIB_DIR" ]] ; then
        printf "WARNING: LIB_DIR $LIB_DIR does not exist as a directory\n" >&2
    fi
    LIB_DIR="${MYDIR}"/../../lib
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    printf "ERROR: LIB_DIR $LIB_DIR not found\n" >&2
    exit 3
fi


## Import dependencies

printf "${ME}: PWD=$PWD  CONF_DIR=$CONF_DIR  SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2
. "$LIB_DIR"/log.sh
. "$LIB_DIR"/${ME}


## Main

log start
dedup_report $*
log done
