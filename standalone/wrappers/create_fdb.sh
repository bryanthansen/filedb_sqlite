#!/bin/sh
# Bryant Hansen

# @brief Create an SQLite-based filesystem database
#
# Features:
#
#     - stores relevant file properties to a database
#     - intended to track and eliminate duplicate information
#     - runs hashes in parallel
#
# Store only directories and files, skip device files, fifos, and other
# special entities for now

set -e

## Constants and Defaults

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="$ME  [ -v ]  [ scan_dir ]"

FILEDB_NAME=filedb.sqlite

# Parallel operation (currently with xargs, though the Perl-based program,
# parallel, has been used successfully)
# Xargs was chosen to minimize the use of non-standard dependencies
#unset PARALLEL_HASHING
PARALLEL_HASHING=1

unset VERBOSE
#VERBOSE=1


## Import Configs

# General config files (fdb.conf) in a search path
# All are imported; generally last-one wins, so order is defined from
# more-default to more-specific
# General order:
#    - global default constants
#    - calculated defaults
#    - application dir conf file
#    - global settings (etc) conf file
#    - user conf file
#    - current directory conf file
#    - command-line option
CONF_DIR_ETC=/etc/fdb
CONF_DIR_HOME=${HOME}/.config/fdb

[[ "$CONF_DIR" ]] || CONF_DIR=.
printf "$ME  CONF_DIR: $CONF_DIR \n" >&2
for d in "$MYDIR" "$CONF_DIR_ETC" "$CONF_DIR_HOME" "$CONF_DIR" ; do
    [[ "$d" ]] || continue
    [[ -d "$d" ]] || continue
    printf "${ME}: checking for config dir ${d}/fdb.conf...\n" >&2
    if [[ -f "$d"/fdb.conf ]] ; then
        . "$d"/fdb.conf
        printf "import conf ${d}/fdb.conf\n" >&2
    fi
done

[[ "$SQL_DIR"  ]] || SQL_DIR=../sql
if [[ ! -d "$SQL_DIR" ]] ; then
    if [[ "$SQL_DIR" ]] ; then
        printf "WARNING: SQL_DIR $SQL_DIR does not exist as a directory\n" >&2
    fi
    SQL_DIR="${MYDIR}/../../sql"
fi
if [[ ! -d "$SQL_DIR" ]] ; then
    printf "ERROR: SQL_DIR $SQL_DIR not found\n" >&2
    exit 2
fi

[[ "$LIB_DIR"  ]] || LIB_DIR=./lib
if [[ ! -d "$LIB_DIR" ]] ; then
    if [[ "$LIB_DIR" ]] ; then
        printf "WARNING: LIB_DIR $LIB_DIR does not exist as a directory\n" >&2
    fi
    LIB_DIR="${MYDIR}"/../../lib
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    printf "ERROR: LIB_DIR $LIB_DIR not found\n" >&2
    exit 3
fi


## Import dependencies

printf "${ME}: PWD=$PWD  CONF_DIR=$CONF_DIR  SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2
. "$LIB_DIR"/"${ME}"


## Process Args - do a pushd if a dir is passed; don't pushd in the lib

if [[ "$1" == "-v" ]] ; then
    VERBOSE=1
    shift
fi
if [[ "$1" ]] ; then
  dir="$1"
  if ! pushd "$dir" > /dev/null; then
      printf "ERROR: could not change directory to $1\n" >&2
      exit 4
  fi
fi


## Main

unset args
[[ "$VERBOSE" ]] && args="-v"

# TODO: determine best strategy for passind SQL_DIR and LIB_DIR to create_fdb
#       Does the child function inherit already?
#       Is it poor style to count on this type of inheritance for passing vars?
#       Alternatively, pass as arguments?  Environmental variables?
create_fdb $args

[[ "$1" ]] && popd > /dev/null
