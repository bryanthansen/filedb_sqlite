#!/bin/bash
# Bryant Hansen

# Description: wrapper script for the status reporting function

set -e

## Constants and Defaults

ME="$(basename "$0")"
MYDIR="$(dirname "$0")"
USAGE="$ME [ db_directory | db_file ]"


## Import Configs

# General config files (fdb.conf) in a search path
# All are imported; generally last-one wins, so order is defined from
# more-default to more-specific
# General order:
#    - global default constants
#    - calculated defaults
#    - application dir conf file
#    - global settings (etc) conf file
#    - user conf file
#    - current directory conf file
#    - command-line option
CONF_DIR_ETC=/etc/fdb
CONF_DIR_HOME=${HOME}/.config/fdb

[[ "$CONF_DIR" ]] || CONF_DIR=.
printf "$ME  CONF_DIR: $CONF_DIR \n" >&2
for d in "$MYDIR" "$CONF_DIR_ETC" "$CONF_DIR_HOME" "$CONF_DIR" ; do
    [[ "$d" ]] || continue
    [[ -d "$d" ]] || continue
    printf "${ME}: checking for config dir ${d}/fdb.conf...\n" >&2
    if [[ -f "$d"/fdb.conf ]] ; then
        . "$d"/fdb.conf
        printf "import conf ${d}/fdb.conf\n" >&2
    fi
done

[[ "$SQL_DIR"  ]] || SQL_DIR=./sql
if [[ ! -d "$SQL_DIR" ]] ; then
    if [[ "$SQL_DIR" ]] ; then
        printf "WARNING: SQL_DIR $SQL_DIR does not exist as a directory\n" >&2
    fi
    SQL_DIR="${MYDIR}/../../sql"
fi
if [[ ! -d "$SQL_DIR" ]] ; then
    printf "ERROR: SQL_DIR $SQL_DIR not found\n" >&2
    exit 2
fi

[[ "$LIB_DIR"  ]] || LIB_DIR=./lib
if [[ ! -d "$LIB_DIR" ]] ; then
    if [[ "$LIB_DIR" ]] ; then
        printf "WARNING: LIB_DIR $LIB_DIR does not exist as a directory\n" >&2
    fi
    LIB_DIR="${MYDIR}"/../../lib
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    printf "ERROR: LIB_DIR $LIB_DIR not found\n" >&2
    exit 3
fi


## Import Dependencies

printf "${ME}: SQL_DIR=$SQL_DIR  LIB_DIR=$LIB_DIR \n" >&2
. "${LIB_DIR}"/log.sh
. "${LIB_DIR}"/${ME}


## Main

if [[ "$2" ]] ; then
    log "ERROR: this script takes 0 or 1 arguments"
    exit 2
fi

dbdir="$(get_dbdir "$1")"
rootdir="$(get_rootdir "$1" "$dbdir")"

gen_report "$dbdir" "$rootdir"
# | tee report_$(date +%Y%m%d_%H%M%S).log
