# Coding Guidelines for the filedb_sqlite project

The initial version is a shell script; it may make more sense to rewrite in another language.  The universality and lack of dependencies is the goal.

Code is generally 80-column; comments may be an exception, but this should also be avoided

Spaces, no tabs

Minimize syntax where possible; avoid optional features unless significant clarity is added.  (ie. use "$foo" instead of "${foo}")

Keep individual scripts as short as possible.  The exception will be the grand stand-alone, auto-generated script.
