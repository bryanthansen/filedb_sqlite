#!/bin/bash
# Bryant Hansen

# Description:
# Run all of the tests in the current folder

set -e

ME="$(basename "$0")"
. ../fdb.conf
echo "${ME}: CONF_DIR=$CONF_DIR" >&2

../common/000_setup_test.sh
../common/001_test_create_fdb.sh
../common/002_test_query.sh
../common/003_test_dedup_report.sh
../common/004_execute_dedup_without_verify.sh

echo "# ${0}: Test Passed.  PWD=$PWD" >&2
