#!/bin/sh
# Bryant Hansen

# Script alternative if make is not available

# Initial 1-liner:
# for d in test/[0-9]* ; do cd $d ; rm -rf tmp/ .filedb_DiskStation_* ; ./test.sh ; cd ../.. ; done

set -e

echo "# PWD: ${PWD}, \$*='$*'" >&2
if [[ -f test.conf ]] ; then
    echo "Read settings from ${PWD}/test.conf" >&2
    . test.conf
fi
p="${PWD}"
for d in [0-9]* ; do
    if [[ -d ${d}/tmp/ ]] ; then
        echo "# ERROR: ${d}/tmp already exists.  Execute the following:" >&2
        echo "rm -rf ${d}/tmp/" >&2
        continue
    fi
    # One platform that I wanted to run this on did not have pushd/popd
    cd $d && ./test.sh || exit $?
    cd "$p"
done
echo -e "\n# ${0} $*: ALL TESTS PASSED\n" >&2
