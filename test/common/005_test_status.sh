#!/bin/sh
# Bryant Hansen

# Description:
# Simple test of checking the status

# Terminate on error
#set -e

## Script defaults and constants

ME="$(basename "$0")"
FILEDB_NAME=filedb.sqlite
SCRIPT_BIN_DIR=../..

CONF_DIR=..
. "$CONF_DIR"/fdb.conf
# LIB_DIR should be set in CONF_DIR
if [[ ! "$LIB_DIR" ]] ; then
    echo "ERROR: must set LIB_DIR in ${CONF_DIR}/fdb.conf" >&2
    exit 2
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    echo "ERROR: LIB_DIR ${LIB_DIR} does not exist" >&2
    exit 3
fi
. "$LIB_DIR"/log.sh
. "$LIB_DIR"/get_dbdir.sh


## Main

echo >&2
log "enter.  PWD=$PWD  CONF_DIR=$CONF_DIR  ARGS=$*"

ret=0
dbdir="$(get_dbdir "$1")" || ret=$?
if [[ "$ret" != 0 ]] ; then
    log "ERROR: failed to get dbdir (arg='${1}')"
    unset dbdir
fi
if [[ ! -d "$dbdir" ]] ; then
    log "ERROR: failed to get dbdir (arg='${1}')"
    unset dbdir
fi

if [[ "$dbdir" ]] ; then
    log "dbdir: $dbdir"

    if [[ ! -d "$dbdir" ]] ; then
        log "ERROR: '${dbdir}' is not a directory"
        exit 6
    fi
    filedb="${dbdir}/${FILEDB_NAME}"
    if [[ ! -f "$filedb" &&  ! -f "$filedb".xz ]] ; then
        log "ERROR: $filedb does not exist"
        exit 7
    fi

    status="$("$SCRIPT_BIN_DIR"/fdb.sh status "$dbdir" 2>&1)"
    ret=$?
    log "status for $dbdir returned code $ret"
    printf "${status}\n" | sed "s/^/\ \ \ \ /"
else
    log "dbdir not specified"
    status="$("$SCRIPT_BIN_DIR"/fdb.sh status 2>&1)"
    ret=$?
    log "status returned code $ret"
    printf "${status}\n" | sed "s/^/\ \ \ \ /"
fi

log "status query complete"
