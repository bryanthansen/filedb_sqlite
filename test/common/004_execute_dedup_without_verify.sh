#!/bin/bash
# Bryant Hansen

# Description:
# Perform a query of the file database

# TODO: verify results

# terminate on error
set -e

#####  Script defaults and constants  ######

ME="$(basename "$0")"
POST_DEDUP_DIR_TREE_STATS_MD5=637df1f23e2d967d5a64619bc1e27ba5
SCRIPT_BIN_DIR=..

#####  Functions  ######

function log_header() {
    printf "$(date +%H:%M:%S.%N):${ME}:" >&2
}

function log() {
    log_header
    printf "$*\n" >&2
}

#####  Main  ######

echo >&2
log enter

dir="$(ls -1td .filedb_* | head -n 1)"
dedup_report="${dir}/duplicate_report.sh"

find_test_pre_dedup="$(find tmp/ -type f -printf "%n %p\n" | sort)"
md5="$(echo -n -e "$find_test_pre_dedup" | sort | md5sum | cut -f 1 -d ' ')"
log "Files in ./tmp/ directory tree\n    # num_links filename"
echo -e "$find_test_pre_dedup" | sed "s/^/\ \ \ \ /" >&2
log "File data md5: $md5"
dirtree_size_pre_dedup="$(du -s ./tmp/ | cut -f 1)"

log "Executing dedup report;   dir usage (bytes): $dirtree_size_pre_dedup"
cat "$dedup_report" | sh
dirtree_size_post_dedup="$(du -s ./tmp/ | cut -f 1)"
log "Dedup execution complete; dir usage (bytes): $dirtree_size_post_dedup"

if [[ $dirtree_size_pre_dedup -le $dirtree_size_post_dedup ]] ; then
    log "ERROR: space not reclaimed via dedup operation. " \
        "Initial size of the directory tree: ${dirtree_size_pre_dedup} bytes;" \
        "Final size of the directory tree: $dirtree_size_post_dedup bytes"
    exit 2
fi

find_test_post_dedup="$(find tmp/ -type f -printf "%n %p\n" | sort)"
md5="$(echo -n -e "$find_test_post_dedup" | sort | md5sum | cut -f 1 -d ' ')"
log "Files in ./tmp/ directory tree\n    # num_links filename"
echo -e "$find_test_post_dedup" | sed "s/^/\ \ \ \ /" >&2
log "  file data md5: $md5"

if [[ "$md5" != $POST_DEDUP_DIR_TREE_STATS_MD5 ]] ; then
    log "ERROR: the directory tree (${md5}) does not match the expected value of $POST_DEDUP_DIR_TREE_STATS_MD5"
    exit 3
fi

#find tmp/ -type f -printf "%i\n" | sort | uniq -c | while read a b ; do echo $a ; done
