#!/bin/bash
# Bryant Hansen

# Description:
# Perform a query of the file database

# TODO: verify results

set -e


## Script defaults and constants

ME="$(basename "$0")"
FILEDB_NAME=filedb.sqlite
MIN_SIZE=8192

QUERY_DATA_MD5=468f14d02fa124b98b57f6d88ad907fd

SCRIPT_BIN_DIR=../..
#SCRIPT_BIN_DIR=../../standalone/wrapper

CONF_DIR=..
. "$CONF_DIR"/fdb.conf
# LIB_DIR should be set in CONF_DIR
if [[ ! "$LIB_DIR" ]] ; then
    echo "ERROR: must set LIB_DIR in ${CONF_DIR}/fdb.conf" >&2
    exit 2
fi
. "$LIB_DIR"/log.sh

sql="
    SELECT hashes.md5, directories.name || '/' || files.name
    FROM files
    LEFT JOIN hashes
    ON files.inode = hashes.inode
    LEFT JOIN directories
    ON files.dirid = directories.id
    WHERE md5 IN (
        SELECT hashes.md5
        FROM files
        LEFT JOIN hashes
        ON files.inode = hashes.inode
        WHERE files.size >= ${MIN_SIZE}
        GROUP BY hashes.md5
        HAVING COUNT(files.inode) > 1
    )
    ORDER BY hashes.md5, files.mtime DESC, files.ctime DESC, files.inode;
"


## Main

echo >&2
log enter

dir="$(ls -1td .filedb_* | head -n 1)"
filedb="${dir}/${FILEDB_NAME}"

log "# Query Test: try to find multiple inodes per MD5 with a size of" \
    "greater than 8kB"

query_results="$(sqlite3 "$filedb" "$sql")"

lines="$(echo -e "$query_results" | wc -l)"
characters="${#query_results}"
md5=$(echo -e "$query_results" | md5sum | cut -f 1 -d ' ')
log "Query Results: $lines lines, $characters characters, md5: $md5"
echo -e "$query_results" | sed "s/^/\ \ \ \ /" >&2

if [[ "$md5" != "$QUERY_DATA_MD5" ]] ; then
    log "WARNING: the query md5 (${md5}) does not match the expected value of $QUERY_DATA_MD5"
    #exit 2
fi

log PASSED
