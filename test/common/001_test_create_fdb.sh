#!/bin/bash
# Bryant Hansen

# Description:
# Simple test script for the Bash/SQLite filedb

# Terminate on error
set -e

## Script defaults and constants

ME="$(basename "$0")"
FILEDB_NAME=filedb.sqlite
SCRIPT_BIN_DIR=../..
#SCRIPT_BIN_DIR=../../standalone/wrapper

CONF_DIR=..
. "$CONF_DIR"/fdb.conf
# LIB_DIR should be set in CONF_DIR
if [[ ! "$LIB_DIR" ]] ; then
    echo "ERROR: must set LIB_DIR in ${CONF_DIR}/fdb.conf" >&2
    exit 2
fi
if [[ ! -d "$LIB_DIR" ]] ; then
    echo "ERROR: LIB_DIR ${LIB_DIR} does not exist" >&2
    exit 3
fi
. "$LIB_DIR"/log.sh


## Main

echo >&2
log "enter.  PWD=$PWD  CONF_DIR=$CONF_DIR"

"$SCRIPT_BIN_DIR"/fdb.sh create .

dir="$(ls -1td .filedb_* | head -n 1)"
if [[ ! -d "$dir" ]] ; then
    printf "ERROR: $dir is not a directory\n" >&2
    exit 4
fi
filedb="${dir}/${FILEDB_NAME}"
if [[ ! -f "$filedb" ]] ; then
    printf "ERROR: $filedb not created\n" >&2
    exit 5
fi
echo -e "# filedb created at $filedb" >&2

log exit
