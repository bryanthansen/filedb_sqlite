#!/bin/bash
# Bryant Hansen

# Set up a test environment with various duplicate files which should be
# replaced by hard links.
# One key challenge is to deal with filenames and all of the special
# characters allowed.  Delimiters must be escaped.

set -e

#####  Script defaults and constants  #####

ME="$(basename "$0")"
TEST="$(basename "$(dirname "$0")")"
CONF_DIR=..

#dd iflag=fullblock if=/dev/random of=.rand_256_bytes.tmp bs=16 count=16
test_dir=tmp
DIR_TREE_HASH=fa17018ddde92581c0eafd86186d5e95

#####  Functions  ######

function log_header() {
    printf "$(date +%H:%M:%S.%N):${TEST}:${ME}:" >&2
}

function log() {
    log_header
    printf "$*\n" >&2
}

function create_dummy_file() {
    # Create a generic block of dummy data for the file
    line="$(
        for m in {1..32} ; do
            printf "%02X, " $m
        done
    )"
    for n in {1..128} ; do
        printf "${line}\n"
    done \
    > "$1"
}

#####  Process Arguments  #####

[[ "$1" ]] && test_dir="$1"

#####  Main  #####

echo "${ME}: CONF_DIR=$CONF_DIR" >&2

mkdir -p "$test_dir"
testfile="${test_dir}/testdata_16k.tmp"
create_dummy_file "$testfile"

echo >&2
log "Set up test environment"
set -o verbose
touch "${test_dir}"/empty_file.db
cp -a "$testfile" "${test_dir}"/first_test.db
cp -a "$testfile" "${test_dir}"/single_quote_test\'.db
cp -a "$testfile" "${test_dir}"/double_quote_test\".db
ln    "$testfile" "${test_dir}"/"hard_link_test.db"
mkdir "${test_dir}"/"single_quote_dir_test'"
mkdir "${test_dir}"/'double_quote_dir_test"'
cp -a "$testfile" "${test_dir}"/"single_quote_dir_test'"/
cp -a "$testfile" "${test_dir}"/double_quote_dir_test\"/
cp -a "$testfile" "${test_dir}"/"file space test.db"
cp -a "$testfile" "${test_dir}"/"trailing single space "

# brief sleep to clearly set the "file space test.db" as being
# the most-recently-changed inode
sleep 0.01
cp -a "$testfile" "${test_dir}"/"trailing double space  "

# in case we want to add any supplementary data
#cp -a ../data     "${test_dir}"/
set +o verbose

find_test_pre_dedup="$(find tmp/ -type f -printf "%n %p\n" | sort)"
md5="$(echo -n -e "$find_test_pre_dedup" | sort | md5sum | cut -f 1 -d ' ')"
log "Files in ./tmp/ directory tree\n    # num_links filename"
echo -e "$find_test_pre_dedup" | sed "s/^/\ \ \ \ /" >&2
log "  file data md5: $md5"

if [[ "$md5" != $DIR_TREE_HASH ]] ; then
    log "ERROR: the hash of the directory tree data (${md5}) does not match " \
        "the expected value of $DIR_TREE_HASH"
    exit 2
fi

log "Test environment setup complete"
