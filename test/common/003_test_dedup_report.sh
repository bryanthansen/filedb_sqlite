#!/bin/bash
# Bryant Hansen

# Description:
# Perform a query of the file database

# TODO: verify results

# terminate on error
set -e

#####  Script defaults and constants  ######

ME="$(basename "$0")"
DEDUP_DATA_MD5=64f0440daf843f14e2b07fe523c428c0
SCRIPT_BIN_DIR=../..
#SCRIPT_BIN_DIR=../../standalone/wrapper

#####  Functions  ######

function log_header() {
    printf "$(date +%H:%M:%S.%N):${ME}:" >&2
}

function log() {
    log_header
    printf "$*\n" >&2
}

#####  Main  ######

echo >&2
log enter

dbdir="$(ls -1td .filedb_* | head -n 1)"
if [[ ! -d "$dbdir" ]] ; then
    log "ERROR: dbdir does not exist as a directory"
fi
dedup_report="${dbdir}/duplicate_report.sh"
rm -f "$dedup_report"

log "# Producing dedup report for dbdir ${dbdir}"
"$SCRIPT_BIN_DIR"/fdb.sh dedup "$dbdir"
log "# ${SCRIPT_BIN_DIR}/fdb.sh dedup ${dbdir} should create $dedup_report"
if [[ -f "$dedup_report" ]] ; then
    dedup_data="$(cat "$dedup_report" | sort)"
else
    log "ERROR: $dedup_report not created when running fdb.sh dedup"
    exit 2
fi

log_header
echo -n "  dedup data size: ${#dedup_data}  lines: " >&2
echo -e -n "$dedup_data" | wc -l >&2

md5="$(echo -e -n "$dedup_data" | sort | md5sum | cut -f 1 -d ' ')"
log "  dedup data md5: $md5"

if [[ "$md5" != $DEDUP_DATA_MD5 ]] ; then
    log "ERROR: the dedup_report md5 (${md5}) does not match the expected" \
        "value of $DEDUP_DATA_MD5"
    exit 2
fi

if [[ -d ../.test.results ]] ; then
    if [[ -f ../.test.results/${md5}.txt ]] ; then
        log "INFO: ../.test.results/${md5}.txt exists"
        touch ../.test.results/${md5}.txt
    else
        cp -a "$dedup_report" ../.test.results/${md5}.txt
    fi
else
    log "WARNING: results log dir does not exist: ../.test.results"
fi

log exit
