#!/bin/bash
# Bryant Hansen

# Description:
# Loop through a series of hard link statements and verify properties of both
# files prior to executing the hard link.
# Do not execute the hard link command if:
#   1) the hashes of the 2 files no longer match
#   2) they are already linked
#
# Announce the before and after storage space and comparisons

# terminate on error
set -e

ME="$(basename "$0")"
POST_DEDUP_DIR_TREE_STATS_MD5=637df1f23e2d967d5a64619bc1e27ba5
SCRIPT_BIN_DIR=../..

CONF_DIR=..
. "$CONF_DIR"/fdb.conf
. "$LIB_DIR"/log.sh
. "$LIB_DIR"/flexcat.sh


echo >&2
log enter

dbdir="$(ls -1td .filedb_* | head -n 1)"
dedup_report="${dbdir}/duplicate_report.sh"

find_test_pre_dedup="$(find tmp/ -type f -printf "%n %p\n" | sort)"
md5="$(echo -n -e "$find_test_pre_dedup" | sort | md5sum | cut -f 1 -d ' ')"
log "Files in ./tmp/ directory tree\n    # num_links filename"
echo -e "$find_test_pre_dedup" | sed "s/^/\ \ \ \ /" >&2
log "File data md5: $md5"
dirtree_size_pre_dedup="$(du -s ./tmp/ | cut -f 1)"
log "Executing dedup report;   dir usage (bytes): $dirtree_size_pre_dedup"

CMD="${SCRIPT_BIN_DIR}/fdb.sh execute"
OPTS=--already-linked-ok
if ! $CMD $OPTS "$dbdir" ; then
    log "ERROR: execute log failed: $CMD $OPTS '$dbdir'"
    if ls "$dbdir"/execute_*.log* > /dev/null 2>&1 ; then
        for f in "$dbdir"/execute_*.log* ; do
            [[ -f "$f" ]] || continue
            echo "f Log: $f"
            flexcat "$f" | sed "s/^/\ \ \ \ /"
            echo ""
        done >&2
    else
        log "ERROR: execution logs not found"
    fi
    exit 4
fi

dirtree_size_post_dedup="$(du -s ./tmp/ | cut -f 1)"
log "Dedup execution complete; dir usage (bytes): $dirtree_size_post_dedup"
if [[ $dirtree_size_pre_dedup -le $dirtree_size_post_dedup ]] ; then
    log "ERROR: space not reclaimed via dedup operation. " \
        "Initial size of the directory tree: ${dirtree_size_pre_dedup} bytes;" \
        "Final size of the directory tree: $dirtree_size_post_dedup bytes"
    exit 5
fi

find_test_post_dedup="$(find tmp/ -type f -printf "%n %p\n" | sort)"
md5="$(echo -n -e "$find_test_post_dedup" | sort | md5sum | cut -f 1 -d ' ')"
log "Files in ./tmp/ directory tree\n    # num_links filename"
echo -e "$find_test_post_dedup" | sed "s/^/\ \ \ \ /" >&2
log "  file data md5: $md5"

if [[ "$md5" != $POST_DEDUP_DIR_TREE_STATS_MD5 ]] ; then
    log "ERROR: the dedup_report md5 (${md5}) does not match the expected" \
        "value of $POST_DEDUP_DIR_TREE_STATS_MD5"
    exit 6
fi

