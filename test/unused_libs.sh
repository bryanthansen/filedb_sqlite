#!/bin/bash
# Bryant Hansen

# Description

# Test that we don't have any unused scripts in our library folder

echo "Executing $0 at $PWD" >&2
for f in ../lib/*.sh ; do
    b="$(basename "$f")"
    if ! grep -q "$b" ../lib/*.sh ../fdb.sh ; then
        echo "$f is not imported" >&2
        exit 1
    fi
done
