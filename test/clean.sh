#!/bin/sh
# Bryant Hansen

(
    rm -f 001_dedup_without_verify/.filedb_*/duplicate_report.sh
    rm -f 001_dedup_without_verify/.filedb_*/filedb.sqlite
    rm -f 002_dedup_with_verify/.filedb_*/duplicate_report.sh
    rm -f 002_dedup_with_verify/.filedb_*/execute_*.log
    rm -f 002_dedup_with_verify/.filedb_*/filedb.sqlite
    rm -f 003_verify_dedup_corruption/.filedb_*/duplicate_report.sh
    rm -f 003_verify_dedup_corruption/.filedb_*/execute_*.log
    rm -f 003_verify_dedup_corruption/.filedb_*/filedb.sqlite
    rm -f 004_no_verify_dedup_corruption/.filedb_*/duplicate_report.sh
    rm -f 004_no_verify_dedup_corruption/.filedb_*/filedb.sqlite
    rmdir 001_dedup_without_verify/.filedb_*
    rmdir 002_dedup_with_verify/.filedb_*
    rmdir 003_verify_dedup_corruption/.filedb_*
    rmdir 004_no_verify_dedup_corruption/.filedb_*
    rm -rf 001_dedup_without_verify/tmp/
    rm -rf 002_dedup_with_verify/tmp/
    rm -rf 003_verify_dedup_corruption/tmp/
    rm -rf 004_no_verify_dedup_corruption/tmp/
) >&2
