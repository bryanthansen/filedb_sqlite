#!/bin/bash
# Bryant Hansen

# Description:
# Run all of the tests in the current folder

ME="$(basename "$0")"

set -e

## Functions

function log_header() {
    printf "$(date +%H:%M:%S.%N):${ME}:" >&2
}

function log() {
    log_header
    printf "$*\n" >&2
}


## Main

. ../fdb.conf
echo "${ME}: CONF_DIR=$CONF_DIR" >&2

../common/000_setup_test.sh
../common/001_test_create_fdb.sh
../common/002_test_query.sh
../common/003_test_dedup_report.sh

# Change on of our destination files prior to executing the dedup
# Now perform the dedup_with_verify with an expected error
mv './tmp/hard_link_test.db' './tmp/.hard_link_test.db.orig'
cp -a './tmp/.hard_link_test.db.orig' './tmp/hard_link_test.db'
echo "new data" >> './tmp/hard_link_test.db'
md5_1=$(cat './tmp/hard_link_test.db' | md5sum | cut -f 1 -d ' ')

log "INFO: directory tree modified; triggering verification failure"
if ../common/004_execute_dedup_with_verify.sh ; then
    log "ERROR: corruption not detected\n"
else
    log "INFO: the previous error was expected.  Test passed."
fi
md5_2=$(cat './tmp/hard_link_test.db' | md5sum | cut -f 1 -d ' ')

# If the MD5sum of the file has changed, it is an error
if [[ "$md5_1" != "$md5_2" ]] ; then
    log "ERROR: Original MD5: ${md5_1}, MD5 after dedup: ${md5_2}. " \
        "Verification failed.  './tmp/hard_link_test.db' was corrupted"
    exit 2
fi

# If there are any links to this file, it is an error
if [[ $(stat --format='%h' './tmp/hard_link_test.db') != 1 ]] ; then
    log "ERROR: Multiple links to './tmp/hard_link_test.db' found.  Verification failed."
    exit 3
fi

if ! xzcat .filedb_*/execute_*.log.xz | grep -q "ERROR: file md5s do not match:" ; then
    log "ERROR: Change to './tmp/hard_link_test.db' not announced in log.  Verification failed."
    exit 4
fi

log "# ${0}: Test Passed.  PWD=$PWD"
