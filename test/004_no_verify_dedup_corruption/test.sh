#!/bin/bash
# Bryant Hansen

# Description:
# Run all of the tests in the current folder

ME="$(basename "$0")"

set -e

#####  Functions  ######

function log_header() {
    printf "$(date +%H:%M:%S.%N):${ME}:" >&2
}

function log() {
    log_header
    printf "$*\n" >&2
}


## Main

. ../fdb.conf
echo "${ME}: CONF_DIR=$CONF_DIR" >&2

../common/000_setup_test.sh
../common/001_test_create_fdb.sh
../common/002_test_query.sh
../common/003_test_dedup_report.sh


# Change on of our destination files prior to executing the dedup
mv './tmp/hard_link_test.db' './tmp/.hard_link_test.db.orig'
cp -a './tmp/.hard_link_test.db.orig' './tmp/hard_link_test.db'
echo "new data" >> './tmp/hard_link_test.db'
md5_1=$(cat './tmp/hard_link_test.db' | md5sum | cut -f 1 -d ' ')
if ../common/004_execute_dedup_without_verify.sh ; then
    log "INFO: corruption not detected\n"
else
    log "INFO: the previous error was expected.  Test passed."
fi
md5_2=$(cat './tmp/hard_link_test.db' | md5sum | cut -f 1 -d ' ')

# If the MD5sum of the file has changed, it is an error
if [[ "$md5_1" == "$md5_2" ]] ; then
    log "ERROR: the md5 ${md5_1} of './tmp/hard_link_test.db' stayed the same during the operation.  A corruption was expected.  Investigate how this was fixed."
    exit 2
fi

# If there are any links to this file, it is an error
if [[ $(stat --format='%h' './tmp/hard_link_test.db') == 1 ]] ; then
    log "ERROR: only 1 link to './tmp/hard_link_test.db' found.  Verification failed.  It was intended to be overwritten in this version of the test"
    exit 3
fi

log "# ${0}: Test Passed.  Args: '$*', PWD=$PWD"
