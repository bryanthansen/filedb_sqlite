#!/bin/bash
# Bryant Hansen

# Description:
# Run all of the tests in the current folder

ME="$(basename "$0")"

set -e

. ../fdb.conf
echo "${ME}: CONF_DIR=$CONF_DIR" >&2

status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status0.log

../common/000_setup_test.sh
status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status1.log

../common/001_test_create_fdb.sh
status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status2.log

../common/002_test_query.sh
status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status3.log

../common/003_test_dedup_report.sh
status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status4.log

execute="$(../common/004_execute_dedup_with_verify.sh)"
printf "${execute}\n" | tee execute.log

status="$(../common/005_test_status.sh)"
printf "${status}\n" | tee status5.log

echo "# ${0}: Test Passed.  PWD=$PWD" >&2
